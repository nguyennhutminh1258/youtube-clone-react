export default {
  name: "video",
  title: "Video",
  type: "document",
  fields: [
    {
      name: "videoTitle",
      title: "Video Title",
      type: "string",
    },
    {
      name: "id",
      title: "ID",
      type: "string",
    },
    {
      name: "videoImage",
      title: "Video Image",
      type: "string",
    },
    {
      name: "videoLength",
      title: "Video Length",
      type: "string",
    },
    {
      name: "videoView",
      title: "Video View",
      type: "string",
    },
    {
      name: "timePublic",
      title: "Time Public",
      type: "string",
    },
    {
      name: "dayPublic",
      title: "Day Public",
      type: "string",
    },
    {
      name: "verifiedIcon",
      title: "Verified Icon",
      type: "string",
    },
    {
      name: "channelImage",
      title: "Channel Image",
      type: "string",
    },
    {
      name: "channelName",
      title: "Channel Name",
      type: "string",
    },
    {
      name: "subscriber",
      title: "Subscriber",
      type: "string",
    },
    {
      name: "likeCounter",
      title: "Like Counter",
      type: "string",
    },
    {
      name: "description",
      title: "Description",
      type: "text",
    },
    {
      name: "comments",
      title: "Comments",
      type: "array",
      of: [{ type: "comment" }],
    },
  ],
};
