export default {
  name: "comment",
  title: "Comment",
  type: "document",
  fields: [
    {
      name: "content",
      title: "Content",
      type: "text",
    },
    {
      name: "postedBy",
      title: "Posted By",
      type: "postedBy",
    },
    {
      name: "dayPost",
      title: "Day Post",
      type: "string",
    },
    {
      name: "publishedTimeText",
      title: "Published Time Text",
      type: "string",
    },
    {
      name: "likeCounter",
      title: "Like Counter",
      type: "number",
    },
  ],
  initialValue: {
    likeCounter: 0,
  },
};
