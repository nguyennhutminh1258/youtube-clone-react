import { createSlice } from "@reduxjs/toolkit";

const uiSlice = createSlice({
  name: "ui",
  initialState: {
    smallSidebar: false,
    toggle: false,
    showInput: false,
    loading: false,
    width: window.innerWidth,
  },
  reducers: {
    setSmallSidebar(state, action) {
      const bool = action.payload;
      return { ...state, smallSidebar: bool };
    },
    setToggle(state, action) {
      const bool = action.payload;
      return { ...state, toggle: bool };
    },
    setShowInput(state, action) {
      const bool = action.payload;
      return { ...state, showInput: bool };
    },
    setLoading(state, action) {
      const bool = action.payload;
      return { ...state, loading: bool };
    },
    setWidth(state, action) {
      const newWidth = action.payload;
      return { ...state, width: newWidth };
    },
  },
});

export const { setSmallSidebar, setToggle, setShowInput, setLoading, setWidth } = uiSlice.actions;

export default uiSlice.reducer;
