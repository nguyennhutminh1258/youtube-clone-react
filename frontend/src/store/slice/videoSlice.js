import { createSlice } from "@reduxjs/toolkit";

const videoSlice = createSlice({
  name: "video",
  initialState: {
    videoList: [],
    hasMore: true,
    loadingVideo: false,
    numberVideo: 20,
  },
  reducers: {
    getVideo() {},
    setVideo(state, action) {
      const videoData = action.payload;
      return { ...state, videoList: [...state.videoList, ...videoData] };
    },
    clearAllVideo() {
      return {
        videoList: [],
        hasMore: true,
        loadingVideo: false,
        numberVideo: 20,
      };
    },
    postComment() {},
    setHasMore(state, action) {
      const bool = action.payload;
      return { ...state, hasMore: bool };
    },
    setLoadingVideo(state, action) {
      const bool = action.payload;
      return { ...state, loadingVideo: bool };
    },
    setNumberVideo(state) {
      return { ...state, numberVideo: state.videoList.length };
    },
  },
});

export const { getVideo, setVideo, postComment, clearAllVideo, setHasMore, setLoadingVideo, setNumberVideo } =
  videoSlice.actions;

export default videoSlice.reducer;
