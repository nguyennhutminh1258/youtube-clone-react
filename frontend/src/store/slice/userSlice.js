import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
  name: "user",
  initialState: {},
  reducers: {
    getUser(action) {},
    setUser(state, action) {
      const userData = action.payload;
      return { ...userData };
    },
    removeUser() {
      return {};
    },
  },
});

export const { getUser, setUser, removeUser } = userSlice.actions;

export default userSlice.reducer;
