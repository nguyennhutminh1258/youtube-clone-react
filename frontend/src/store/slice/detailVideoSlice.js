import { createSlice } from "@reduxjs/toolkit";

const videoDetailSlice = createSlice({
  name: "detail",
  initialState: {},
  reducers: {
    getDetailVideo() {},
    setDetailVideo(state, action) {
      const videoData = action.payload;
      return { ...videoData };
    },
  },
});

export const { getDetailVideo, setDetailVideo } = videoDetailSlice.actions;

export default videoDetailSlice.reducer;
