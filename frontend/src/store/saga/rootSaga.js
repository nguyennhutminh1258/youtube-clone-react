import { takeLatest } from "redux-saga/effects";
import { getDetailVideo } from "../slice/detailVideoSlice";
import { setUser } from "../slice/userSlice";
import { getVideo, postComment } from "../slice/videoSlice";
import { handleGetDetailVideo } from "./handlers/handleGetDetailVideo";
import { handleGetVideo } from "./handlers/handleGetVideo";
import { handlePostComment } from "./handlers/handlePostComment";
import { handlePostUser } from "./handlers/handlePostUser";

export function* watcherSaga() {
  yield takeLatest(getVideo.type, handleGetVideo);
  yield takeLatest(setUser.type, handlePostUser);
  yield takeLatest(postComment.type, handlePostComment);
  yield takeLatest(getDetailVideo.type, handleGetDetailVideo);
}
