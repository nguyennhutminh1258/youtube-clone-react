import { v4 as uuidv4 } from "uuid";
import { client } from "../../../libs/sanity";

export function requestPostComment(doc) {
  const { id, content, userID, publishedTimeText, dayPost } = doc;
  return client
    .patch(id)
    .setIfMissing({ comments: [] })
    .insert("before", "comments[0]", [
      {
        content: content,
        _type: "comment",
        _key: uuidv4(),
        postedBy: {
          _type: "postedBy",
          _ref: userID,
        },
        dayPost: dayPost,
        publishedTimeText: publishedTimeText,
        likeCounter: 0,
      },
    ])
    .commit()
    .then(() => {
      console.log("Add Comment Successfully");
    });
}
