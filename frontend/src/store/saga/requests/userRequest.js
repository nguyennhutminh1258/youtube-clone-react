import { client } from "../../../libs/sanity";

// export function requestGetUser(id) {
//   return client.fetch(`*[_type == "user" && _id == ${id}]`);
// }

export function requestPostUser(doc) {
  return client.createIfNotExists(doc).then((res) => {
    console.log("User was created (or was already present)", res);
  });
}
