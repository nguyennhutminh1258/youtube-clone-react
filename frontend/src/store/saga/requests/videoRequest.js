import { client } from "../../../libs/sanity";

export function requestGetVideo(doc) {
  const { start, end } = doc;
  return client.fetch(
    `*[_type == "video"][${start}...${end}]{
    _id,
    id,
    videoTitle,
    videoImage,
    videoLength,
    videoView,
    timePublic,
    dayPublic,
    verifiedIcon,
    channelImage,
    channelName,
    subscriber,
    likeCounter,
    description,
    comments[] {
      _key,
      content,
      postedBy -> {
        _id,
        userName,
        image
      },
      dayPost,
      publishedTimeText,
      likeCounter
    }
  }`
  );
}
