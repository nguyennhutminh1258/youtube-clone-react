import { client } from "../../../libs/sanity";

export function requestGetDetailVideo(doc) {
  const { id } = doc;
  return client.fetch(`*[_type == "video" && _id == '${id}']{
    _id,
    id,
    videoTitle,
    videoImage,
    videoLength,
    videoView,
    timePublic,
    dayPublic,
    verifiedIcon,
    channelImage,
    channelName,
    subscriber,
    likeCounter,
    description,
    comments[] {
      _key,
      content,
      postedBy -> {
        _id,
        userName,
        image
      },
      dayPost,
      publishedTimeText,
      likeCounter
    }
  }`);
}
