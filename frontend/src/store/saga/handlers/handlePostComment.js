import { call, put } from "redux-saga/effects";
import { getDetailVideo } from "../../slice/detailVideoSlice";
import { setLoading } from "../../slice/uiSlice";
import { requestPostComment } from "../requests/commentRequest";

export function* handlePostComment(action) {
  yield put(setLoading(true));
  try {
    yield call(requestPostComment, action.payload);
    yield put(getDetailVideo(action.payload));
  } catch (error) {
    console.log(error);
  }
  yield put(setLoading(false));
}
