import { call, put } from "redux-saga/effects";
import { setHasMore, setLoadingVideo, setNumberVideo, setVideo } from "../../slice/videoSlice";
import { requestGetVideo } from "../requests/videoRequest";

export function* handleGetVideo(action) {
  yield put(setLoadingVideo(true));
  try {
    const response = yield call(requestGetVideo, action.payload);
    yield put(setVideo(response));
    yield put(setHasMore(response.length > 0));
    yield put(setLoadingVideo(false));
    yield put(setNumberVideo());
  } catch (error) {
    console.log(error);
  }
}
