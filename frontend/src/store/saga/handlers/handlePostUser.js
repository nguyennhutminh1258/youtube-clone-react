import { call } from "redux-saga/effects";
import { requestPostUser } from "../requests/userRequest";

export function* handlePostUser(action) {
  try {
    yield call(requestPostUser, action.payload);
  } catch (error) {
    console.log(error);
  }
}
