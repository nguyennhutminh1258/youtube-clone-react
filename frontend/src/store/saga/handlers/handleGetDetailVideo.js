import { call, put } from "redux-saga/effects";
import { setDetailVideo } from "../../slice/detailVideoSlice";
import { requestGetDetailVideo } from "../requests/detailVideoRequest";

export function* handleGetDetailVideo(action) {
  try {
    const response = yield call(requestGetDetailVideo, action.payload);
    yield put(setDetailVideo({ ...response }));
  } catch (error) {
    console.log(error);
  }
}
