import { combineReducers, configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { watcherSaga } from "./saga/rootSaga";
import detailReducer from "./slice/detailVideoSlice";
import uiReducer from "./slice/uiSlice";
import userSlice from "./slice/userSlice";
import videoReducer from "./slice/videoSlice";

const sagaMiddleware = createSagaMiddleware();
const middleware = [sagaMiddleware];

const reducer = combineReducers({
  video: videoReducer,
  detail: detailReducer,
  ui: uiReducer,
  user: userSlice,
});

const store = configureStore({ reducer, middleware });

sagaMiddleware.run(watcherSaga);

export default store;
