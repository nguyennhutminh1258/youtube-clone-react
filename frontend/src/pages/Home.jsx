import { makeStyles } from "@material-ui/core";
import Box from "@mui/material/Box";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Chips from "../components/chip";
import Header from "../components/header";
import Sidebar from "../components/sidebar";
import SidebarShow from "../components/sidebar-show";
import SkeletonLoader from "../components/skeleton-loader";
import Video from "../components/video";
import { clearAllVideo, getVideo } from "../store/slice/videoSlice";

const useStyles = makeStyles({
  main: {
    width: "100%",
    height: "calc(100% - 56px)",
    display: "flex",
    backgroundColor: "rgba(255, 255, 255, 0.98)",
  },
  right: {
    position: "relative",
    width: "calc(100% - 240px)",
    height: "100%",
    backgroundColor: "#f9f9f9",
    "&.small": {
      width: "calc(100% - 72px)",
      "@media (max-width: 785px)": {
        width: "100%",
      },
    },
    "@media (max-width: 1315px)": {
      width: "calc(100% - 72px)",
    },
    "@media (max-width: 785px)": {
      width: "100%",
    },
  },
  content: {
    position: "relative",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    marginTop: "24px",
    width: "100%",
  },
  skeletonContainer: {
    display: "flex",
    justifyContent: "center",
    width: "100%",
  },
  skeletonSection: {
    position: "relative",
    width: "100%",
    display: "flex",
    justifyContent: "center",
    flexWrap: "wrap",
    maxWidth: "calc(4 * 360px + 64px)",
    margin: "0 16px",
    "&.small": {
      maxWidth: "calc(5 * 360px + 64px)",
    },
  },
});

const Home = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const smallSidebar = useSelector((state) => state.ui.smallSidebar);
  const width = useSelector((state) => state.ui.width);
  const loadingVideo = useSelector((state) => state.video.loadingVideo);
  const hasMore = useSelector((state) => state.video.hasMore);
  const numberVideo = useSelector((state) => state.video.numberVideo);

  useEffect(() => {
    dispatch(clearAllVideo());
    dispatch(
      getVideo({
        start: 0,
        end: 20,
      })
    );
  }, [dispatch]);

  window.onscroll = function () {
    if (
      window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight &&
      hasMore &&
      typeof numberVideo != "undefined"
    ) {
      dispatch(
        getVideo({
          start: numberVideo,
          end: numberVideo + 20,
        })
      );
    }
  };

  return (
    <>
      <Header />
      <SidebarShow active={true} />
      <Box className={classes.main}>
        <Sidebar />
        <Box className={`${classes.right} ${smallSidebar ? "small" : ""}`}>
          <Chips />
          <Box className={classes.content}>
            <Video />
            {loadingVideo && (
              <Box className={classes.skeletonContainer}>
                <Box className={`${classes.skeletonSection} ${smallSidebar && width > 1690 ? "small" : ""}`}>
                  {smallSidebar && width > 1690 ? (
                    <>
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                    </>
                  ) : (
                    <>
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                      <SkeletonLoader />
                    </>
                  )}
                </Box>
              </Box>
            )}
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default Home;
