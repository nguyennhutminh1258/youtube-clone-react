import { makeStyles } from "@material-ui/core";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import { useEffect, useState } from "react";
import ReactPlayer from "react-player";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Comment from "../components/comment";
import Header from "../components/header";
import SidebarShow from "../components/sidebar-show";
import VideoInfo from "../components/video-info";
import VideoRecommend from "../components/video-recommend";
import { getDetailVideo } from "../store/slice/detailVideoSlice";
import { clearAllVideo, getVideo } from "../store/slice/videoSlice";

const useStyles = makeStyles({
  container: {
    width: "100%",
    height: "100%",
    backgroundColor: "#f9f9f9",
  },
  main: {
    display: "flex",
    flexDirection: "row",
    margin: "0 auto",
    justifyContent: "center",
    maxWidth: "calc(1280px + 402px + 3 * 24px)",
  },
  left: {
    maxWidth: "calc((100vh - (56px + 24px + 136px)) * (16 / 9))",
    minWidth: "calc(480px * (16 / 9))",
    paddingTop: "24px",
    paddingRight: "24px",
    marginLeft: "24px",
    flex: 1,
    "@media (max-width: 1350px)": {
      minWidth: "calc(360px * (16 / 9))",
    },
    "@media (max-width: 1030px)": {
      minWidth: "calc(240px * (16 / 9))",
    },
  },
  videoContainer: {
    position: "relative",
    width: "100%",
    overflow: "hidden",
    paddingTop: "56.25%",
  },
  video: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    width: "100%",
    height: "100%",
    border: "none",
  },
  videoRecommend: {
    display: "flex",
    flexDirection: "column",
  },
  right: {
    paddingTop: "24px",
    paddingRight: "24px",
    width: "402px",
    minWidth: "300px",
    "@media (max-width: 1030px)": {
      display: "none",
    },
  },
  btnShowMore: {
    width: "100%",
    "& button": {
      height: "36px",
      width: "100%",
      textTransform: "uppercase",
      outline: "none",
      fontSize: "14px",
      fontWeight: 500,
      lineHeight: "36px",
      letterSpacing: "0.5px",
      verticalAlign: "middle",
      color: "#056fd4",
      padding: "0 15px",
      backgroundColor: "#f9f9f9",
      cursor: "pointer",
      border: "1px solid rgba(0, 0, 0, 0.1)",
      "&:hover": {
        backgroundColor: "#f0f0f0",
      },
    },
  },
});

const DetailVideo = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const { id } = useParams();
  const [isShowVideoRecommend, setIsShowVideoRecommend] = useState(false);

  const width = useSelector((state) => state.ui.width);
  const detailVideo = useSelector((state) => state.detail);
  const loadingVideo = useSelector((state) => state.video.loadingVideo);
  const numberVideo = useSelector((state) => state.video.numberVideo);
  const hasMore = useSelector((state) => state.video.hasMore);

  const showMoreVideo = () => {
    dispatch(
      getVideo({
        start: numberVideo,
        end: numberVideo + 20,
      })
    );
  };

  useEffect(() => {
    setIsShowVideoRecommend(width <= 1030);
  }, [width]);

  useEffect(() => {
    document.title = detailVideo[0]?.videoTitle;
  }, [detailVideo, dispatch]);

  useEffect(() => {
    dispatch(
      getDetailVideo({
        id: id,
      })
    );
  }, [id, dispatch]);

  useEffect(() => {
    dispatch(clearAllVideo());
    dispatch(
      getVideo({
        start: 0,
        end: 20,
      })
    );
  }, [dispatch]);

  window.onscroll = function () {
    if (
      window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight &&
      hasMore &&
      width >= 1030 &&
      typeof numberVideo != "undefined"
    ) {
      dispatch(
        getVideo({
          start: numberVideo,
          end: numberVideo + 20,
        })
      );
    }
  };

  return (
    <>
      <Header detail={true} />
      <SidebarShow active={false} />
      <Box className={classes.container}>
        <Box className={classes.main}>
          <Box className={classes.left}>
            <Box className={classes.videoContainer}>
              <ReactPlayer
                className={classes.video}
                key={`${detailVideo[0]?._id}`}
                url={`https://www.youtube.com/watch?v=${detailVideo[0]?.id}`}
                width="100%"
                height="100%"
                playing={true}
                controls={true}
              />
            </Box>
            <Box className={classes.below}>
              <Box className={classes.videoInfoContainer}>
                <VideoInfo
                  key={id}
                  videoTitle={detailVideo[0]?.videoTitle}
                  videoView={detailVideo[0]?.videoView}
                  dayPublic={detailVideo[0]?.dayPublic}
                  verifiedIcon={detailVideo[0]?.verifiedIcon}
                  channelImage={detailVideo[0]?.channelImage}
                  channelName={detailVideo[0]?.channelName}
                  subscriber={detailVideo[0]?.subscriber}
                  likeCounter={detailVideo[0]?.likeCounter}
                  description={detailVideo[0]?.description}
                />
              </Box>
              {isShowVideoRecommend && (
                <Box className={classes.videoRecommend}>
                  <VideoRecommend id={detailVideo[0]?._id} />
                  {loadingVideo ? (
                    <Box sx={{ display: "flex", justifyContent: "center" }}>
                      <CircularProgress
                        className={classes.loader}
                        thickness={4}
                        style={{
                          color: "gray",
                          height: "35px",
                          width: "35px",
                        }}
                      />
                    </Box>
                  ) : (
                    width <= 1030 &&
                    numberVideo >= 20 &&
                    hasMore && (
                      <Box className={classes.btnShowMore} onClick={() => showMoreVideo()}>
                        <button>Hiện thêm</button>
                      </Box>
                    )
                  )}
                </Box>
              )}
              <Box className={classes.commentContainer}>
                <Comment commentCount={detailVideo[0]?.comments?.length} comment={detailVideo[0]?.comments} />
              </Box>
            </Box>
          </Box>
          <Box className={classes.right}>
            <VideoRecommend _id={detailVideo[0]?._id} />
            {loadingVideo && (
              <Box sx={{ display: "flex", justifyContent: "center" }}>
                <CircularProgress
                  className={classes.loader}
                  thickness={4}
                  style={{
                    color: "gray",
                    height: "35px",
                    width: "35px",
                  }}
                />
              </Box>
            )}
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default DetailVideo;
