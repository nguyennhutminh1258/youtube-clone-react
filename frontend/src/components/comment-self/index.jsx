import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import { gapi } from "gapi-script";
import moment from "moment";
import "moment/locale/vi";
import { useEffect, useRef, useState } from "react";
import { Field, Form } from "react-final-form";
import GoogleLogin from "react-google-login";
import { useDispatch, useSelector } from "react-redux";
import { setUser } from "../../store/slice/userSlice";
import { postComment } from "../../store/slice/videoSlice";
import { useStyles } from "./index.style";

const CommentSelf = () => {
  moment.locale("vi");
  const classes = useStyles();
  const dispatch = useDispatch();

  const [isShowBtnComment, setIsShowBtnComment] = useState();

  const inputRef = useRef();
  const textAreaRef = useRef();

  const loading = useSelector((state) => state.ui.loading);
  const user = useSelector((state) => state.user);
  const detailVideo = useSelector((state) => state.detail);

  const userInfo =
    localStorage.getItem("user") !== "undefined"
      ? JSON.parse(localStorage.getItem("user"))
      : localStorage.clear;
  // console.log(user); //user._id
  // console.log(userInfo); //userInfo.googleId

  const responseGoogle = (response) => {
    localStorage.setItem("user", JSON.stringify(response.profileObj));
    const { name, googleId, imageUrl } = response.profileObj;

    const doc = {
      _id: googleId,
      _type: "user",
      userName: name,
      image: imageUrl,
    };
    dispatch(setUser(doc));
  };

  const handleOnCancel = (form) => {
    setIsShowBtnComment(false);
    form.change("content", undefined);
    textAreaRef.current.style.height = "20px";
  };

  const onSubmit = async (values, form) => {
    try {
      if (values.content) {
        dispatch(postComment(values));
        form.change("content", undefined);
        setIsShowBtnComment(false);
        textAreaRef.current.style.height = "20px";
      } else {
        console.log("Content is empty!");
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleOnFocusInput = () => {
    if (userInfo) {
      if (inputRef) {
        setIsShowBtnComment(true);
        inputRef.current.style.backgroundSize = "100% 1.6px";
        inputRef.current.style.transition = "0.3s";
      }
    }
  };

  const handleOnBlurInput = () => {
    if (inputRef) {
      inputRef.current.style.backgroundSize = "0% 1.5px";
      inputRef.current.style.transition = "0s";
    }
  };

  const handleOnComment = () => {
    textAreaRef.current.style.height = "20px";
    textAreaRef.current.style.height = textAreaRef.current.scrollHeight + "px";
  };

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: process.env.REACT_APP_GOOGLE_API_TOKEN,
        scope: "email",
      });
    }

    gapi.load("client:auth2", start);
  }, []);

  if (loading) {
    return (
      <Box sx={{ display: "flex", justifyContent: "center" }}>
        <CircularProgress
          className={classes.loader}
          thickness={4}
          style={{
            color: "gray",
            height: "35px",
            width: "35px",
          }}
        />
      </Box>
    );
  }

  return (
    <Box className={classes.container}>
      <Box className={classes.imageAuthor}>
        {Object.keys(user).length !== 0 || userInfo ? (
          <img src={userInfo ? userInfo?.imageUrl : user?.image} alt="" />
        ) : (
          <img src="https://yt3.ggpht.com/a/default-user=s48-c-k-c0x00ffffff-no-rj" alt="" />
        )}
      </Box>
      <Form
        onSubmit={onSubmit}
        initialValues={{
          content: "",
          id: detailVideo[0]?._id,
          userID: userInfo ? userInfo.googleId : user._id,
          publishedTimeText: "vài giây trước",
          dayPost: moment().format("MMMM Do YYYY, h:mm:ss a"),
        }}
        render={({ handleSubmit, form, pristine, submitting, values }) => (
          <form onSubmit={handleSubmit} style={{ width: "100%" }}>
            <Box className={classes.inputAreaContainer}>
              <Box className={classes.inputArea} ref={inputRef}>
                {userInfo ? (
                  <Field
                    ref={textAreaRef}
                    name="content"
                    type="text"
                    component="textarea"
                    autoComplete="off"
                    placeholder="Viết bình luận..."
                    onFocus={() => handleOnFocusInput()}
                    onBlur={() => handleOnBlurInput()}
                    onInput={() => handleOnComment()}
                  />
                ) : (
                  <GoogleLogin
                    clientId={process.env.REACT_APP_GOOGLE_API_TOKEN}
                    render={(renderProps) => (
                      <Field
                        name="content"
                        type="text"
                        component="textarea"
                        autoComplete="off"
                        placeholder="Viết bình luận..."
                        onClick={renderProps.onClick}
                        disabled={renderProps.disabled}
                      />
                    )}
                    onSuccess={(response) => responseGoogle(response)}
                    onFailure={(response) => responseGoogle(response)}
                    cookiePolicy="single_host_origin"
                  />
                )}
              </Box>
              {isShowBtnComment && (
                <Box className={classes.btnSubmitContainer}>
                  <button type="button" className={classes.btnCancel} onClick={() => handleOnCancel(form)}>
                    Hủy
                  </button>
                  <button
                    type="submit"
                    disabled={submitting || pristine}
                    // check content is null/ undefined/ empty
                    className={`${values.content ? classes.btnSubmit : classes.btnPristine}`}>
                    Bình luận
                  </button>
                </Box>
              )}
            </Box>
          </form>
        )}
      />
    </Box>
  );
};

export default CommentSelf;
