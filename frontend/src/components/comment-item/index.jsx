import Box from "@mui/material/Box";
import PropTypes from "prop-types";
import { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { ReactComponent as ThumbsDown } from "../../assets/images/detail/thumbs-down.svg";
import { ReactComponent as ThumbsUp } from "../../assets/images/detail/thumbs-up.svg";
import { useStyles } from "./index.style";

const CommentItem = ({ _key, imageAuthor, authorName, publishedTimeText, commentContent, likeCounter }) => {
  const classes = useStyles();

  const commentContentRef = useRef();
  const expandBtnRef = useRef();
  const hideBtnRef = useRef();
  const [isShowBtnComment, setIsShowBtnComment] = useState(false);
  const width = useSelector((state) => state.ui.width);

  const handleExpandComment = () => {
    commentContentRef.current.style.WebkitLineClamp = "unset";
    expandBtnRef.current.style.display = "none";
    hideBtnRef.current.style.display = "block";
  };

  const handleHideComment = () => {
    commentContentRef.current.style.WebkitLineClamp = "4";
    expandBtnRef.current.style.display = "block";
    hideBtnRef.current.style.display = "none";
  };

  const isEllipsisComment = () => {
    setIsShowBtnComment(
      commentContentRef.current?.scrollHeight > commentContentRef.current?.clientHeight &&
        commentContentRef.current?.style.WebkitLineClamp !== "unset"
    );
  };

  useEffect(() => {
    isEllipsisComment();
    if (commentContentRef.current?.style.WebkitLineClamp === "unset") {
      commentContentRef.current.style.WebkitLineClamp = "4";
    }
  }, [width]);

  return (
    <Box className={classes.container}>
      <Box className={classes.commentItem}>
        <Box className={classes.imageAuthor}>
          <img src={imageAuthor} alt="" />
        </Box>
        <Box className={classes.commentMain}>
          <Box className={classes.header}>
            <Box className={classes.headerAuthor}>
              <Box className={classes.authorName}>
                <span>{authorName}</span>
              </Box>
              <Box className={classes.publicTimeText}>
                <span>{publishedTimeText}</span>
              </Box>
            </Box>
          </Box>
          <Box className={classes.content}>
            <span className={classes.commentContent} ref={commentContentRef}>
              {commentContent}
            </span>
          </Box>
          {isShowBtnComment && (
            <>
              <span
                className={`${classes.expandComment} ${isShowBtnComment ? "showBtnComment" : ""}`}
                onClick={() => handleExpandComment()}
                ref={expandBtnRef}>
                Đọc thêm
              </span>
              <span
                className={`${classes.hideComment} ${isShowBtnComment ? "showBtnComment" : ""}`}
                onClick={() => handleHideComment()}
                ref={hideBtnRef}>
                Ẩn bớt
              </span>
            </>
          )}
          <Box className={classes.action}>
            <Box className={classes.toolBars}>
              <Box className={classes.iconLike}>
                <ThumbsUp />
              </Box>
              <span className={classes.likeNum}>{likeCounter}</span>
              <Box className={classes.iconUnlike}>
                <ThumbsDown />
              </Box>
              <Box className={classes.replyBtnContainer}>
                <Box className={classes.replyBtn}>
                  <span>Phản hồi</span>
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

CommentItem.propTypes = {
  _key: PropTypes.string,
  imageAuthor: PropTypes.string,
  authorName: PropTypes.string,
  publishedTimeText: PropTypes.string,
  commentContent: PropTypes.string,
  likeCounter: PropTypes.number,
};

export default CommentItem;
