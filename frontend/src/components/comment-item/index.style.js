import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
  container: {
    marginBottom: "16px",
  },
  commentItem: {
    display: "flex",
    flexDirection: "row",
  },
  imageAuthor: {
    marginRight: "16px",
    flex: "none",
    "& img": {
      width: "40px",
      height: "40px",
      borderRadius: "50%",
    },
  },
  commentMain: {
    minWidth: 0,
    flex: 1,
    display: "flex",
    flexDirection: "column",
  },
  header: {
    marginBottom: "2px",
  },
  headerAuthor: {
    display: "flex",
    flexDirection: "row",
  },
  authorName: {
    marginRight: "4px",
    marginBottom: "2px",
    color: "#0f0f0f",
    fontSize: "13px",
    fontWeight: 500,
    lineHeight: "18px",
  },
  publicTimeText: {
    color: "#606060",
    fontSize: "12px",
    fontWeight: 400,
    lineHeight: "18px",
  },
  comment: {
    marginRight: "8px",
    flex: "0 0 auto",
  },
  commentContent: {
    color: "#0f0f0f",
    display: "-webkit-box",
    overflow: "hidden",
    "-webkit-box-orient": "vertical",
    "-webkit-line-clamp": 4,
    fontSize: "14px",
    lineHeight: "20px",
    fontWeight: 400,
    whiteSpace: "pre-wrap",
    textOverflow: "ellipsis",
  },
  expandComment: {
    display: "none",
    fontSize: "14px",
    lineHeight: "20px",
    fontWeight: 500,
    color: "#606060",
    cursor: "pointer",
    "&:hover": {
      textDecoration: "underline",
    },
    "&.showBtnComment": {
      display: "block",
    },
  },
  hideComment: {
    display: "none",
    fontSize: "14px",
    lineHeight: "20px",
    fontWeight: 500,
    color: "#606060",
    cursor: "pointer",
    "&:hover": {
      textDecoration: "underline",
    },
  },
  action: {
    marginTop: "4px",
    minHeight: "16px",
    color: "#606060",
  },
  toolBars: {
    alignItems: "center",
    display: "flex",
    flexDirection: "row",
  },
  iconLike: {
    marginLeft: "-8px",
    padding: "0 6px",
    cursor: "pointer",
  },
  likeNum: {
    marginRight: "8px",
    fontSize: "12px",
    lineHeight: "18px",
    fontWeight: 400,
  },
  iconUnlike: {
    padding: "0 6px",
    cursor: "pointer",
  },
  replyBtnContainer: {
    marginLeft: "8px",
  },
  replyBtn: {
    height: "32px",
    borderRadius: "16px",
    padding: "0 12px",
    cursor: "pointer",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    "&:hover": {
      backgroundColor: "#f0f0f0",
    },
    "& span": {
      color: "#0f0f0f",
      fontSize: "14px",
      lineHeight: "18px",
      fontWeight: 500,
    },
  },
});
