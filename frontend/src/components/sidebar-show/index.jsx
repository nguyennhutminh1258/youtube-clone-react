import Box from "@mui/material/Box";
import PropTypes from "prop-types";
import { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { ReactComponent as IconBar } from "../../assets/images/icon-bar.svg";
import { ReactComponent as DiaryIcon } from "../../assets/images/sidebar/diary-icon.svg";
import { ReactComponent as ExploreIcon } from "../../assets/images/sidebar/explore-icon.svg";
import { ReactComponent as FeedbackIcon } from "../../assets/images/sidebar/feedback-icon.svg";
import GamingIcon from "../../assets/images/sidebar/gaming-icon.jpg";
import { ReactComponent as HomeIcon } from "../../assets/images/sidebar/home-icon.svg";
import { ReactComponent as LibraryIcon } from "../../assets/images/sidebar/library-icon.svg";
import MusicIcon from "../../assets/images/sidebar/music-icon.jpg";
import NewsIcon from "../../assets/images/sidebar/news-icon.jpg";
import { ReactComponent as PlusIcon } from "../../assets/images/sidebar/plus-icon.svg";
import { ReactComponent as SettingIcon } from "../../assets/images/sidebar/setting-icon.svg";
import { ReactComponent as ShortIcon } from "../../assets/images/sidebar/short-icon.svg";
import SportIcon from "../../assets/images/sidebar/sport-icon.jpg";
import { ReactComponent as SubChannelIcon } from "../../assets/images/sidebar/sub-channel-icon.svg";
import { ReactComponent as SupportIcon } from "../../assets/images/sidebar/support-icon.svg";
import VideoIcon from "../../assets/images/sidebar/video-icon.jpg";
import { ReactComponent as VideoWatchedIcon } from "../../assets/images/sidebar/video-watched-icon.svg";
import { ReactComponent as YoutubeKidsIcon } from "../../assets/images/sidebar/youtube-kids-icon.svg";
import { ReactComponent as YoutubeMusicIcon } from "../../assets/images/sidebar/youtube-music-icon.svg";
import { ReactComponent as YoutubeTvIcon } from "../../assets/images/sidebar/youtube-tv-icon.svg";
import { ReactComponent as LogoYoutube } from "../../assets/images/youtube-logo.svg";
import { setToggle } from "../../store/slice/uiSlice";
import SidebarItem from "../sidebar-item";
import { useStyles } from "./index.style";

const SidebarShow = ({ active }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const sidebarRef = useRef();
  const toggle = useSelector((state) => state.ui.toggle);
  const width = useSelector((state) => state.ui.width);

  const userInfo =
    localStorage.getItem("user") !== "undefined"
      ? JSON.parse(localStorage.getItem("user"))
      : localStorage.clear;

  const handleCloseSidebar = () => {
    dispatch(setToggle(false));
  };

  const onTouchStart = () => {
    sidebarRef.current.style.overflowY = "auto";
  };

  useEffect(() => {
    if (width > 1315) {
      dispatch(setToggle(false));
    }
  }, [width, dispatch]);

  return (
    <Box className={classes.container}>
      <Box className={`${classes.lockScreen} ${toggle ? "toggle" : ""}`} onClick={handleCloseSidebar}></Box>
      <Box
        className={`${classes.sidebarContainer} ${toggle ? "toggle" : ""}`}
        onTouchStart={onTouchStart}
        ref={sidebarRef}>
        <Box className={classes.sidebar}>
          <Box className={classes.sidebarHeader}>
            <Box className={classes.menu}>
              <Box className={classes.iconBar} onClick={() => handleCloseSidebar()}>
                <IconBar />
              </Box>
              <Box>
                <LogoYoutube className={classes.logo} />
              </Box>
            </Box>
          </Box>

          <Box className={classes.sidebarLists}>
            <Link to="/" style={{ textDecoration: "none", color: "#000000" }}>
              <SidebarItem icon={<HomeIcon />} title={"Trang chủ"} active={active} toggle={toggle} />
            </Link>
            <SidebarItem icon={<ExploreIcon />} title={"Khám phá"} toggle={toggle} />
            <SidebarItem icon={<ShortIcon />} title={"Shorts"} toggle={toggle} />
            <SidebarItem icon={<SubChannelIcon />} title={"Kênh đăng ký"} toggle={toggle} />
          </Box>

          <Box className={classes.sidebarLists}>
            <SidebarItem icon={<LibraryIcon />} title={"Thư viện"} toggle={toggle} />
            <SidebarItem icon={<VideoWatchedIcon />} title={"Video đã xem"} toggle={toggle} />
          </Box>

          {!userInfo && (
            <Box className={classes.sidebarLists}>
              <SidebarItem loginSection={true} toggle={toggle} />
            </Box>
          )}

          <Box className={classes.sidebarLists}>
            <Box className={classes.sidebarTitle}>Khám phá</Box>
            <SidebarItem icon={<img src={MusicIcon} alt="music-icon" />} title={"Âm nhạc"} toggle={toggle} />
            <SidebarItem icon={<img src={SportIcon} alt="music-icon" />} title={"Thể thao"} toggle={toggle} />
            <SidebarItem
              icon={<img src={GamingIcon} alt="music-icon" />}
              title={"Trò chơi"}
              toggle={toggle}
            />
            <SidebarItem icon={<img src={NewsIcon} alt="music-icon" />} title={"Tin tức"} toggle={toggle} />
            <SidebarItem
              icon={<img src={VideoIcon} alt="music-icon" />}
              title={"Video 360°"}
              toggle={toggle}
            />
          </Box>

          <Box className={classes.sidebarLists}>
            <SidebarItem icon={<PlusIcon />} title={"Xem qua các kênh"} toggle={toggle} />
          </Box>

          <Box className={classes.sidebarLists}>
            <Box className={classes.sidebarTitle}>Dịch vụ khác của Youtube</Box>
            <SidebarItem icon={<YoutubeMusicIcon />} title={"Youtube Music"} toggle={toggle} />
            <SidebarItem icon={<YoutubeKidsIcon />} title={"Youtube Kids"} toggle={toggle} />
            <SidebarItem icon={<YoutubeTvIcon />} title={"Youtube TV"} toggle={toggle} />
          </Box>

          <Box className={classes.sidebarLists}>
            <SidebarItem icon={<SettingIcon />} title={"Cài đặt"} toggle={toggle} />
            <SidebarItem icon={<DiaryIcon />} title={"Nhật kí báo cáo"} toggle={toggle} />
            <SidebarItem icon={<SupportIcon />} title={"Trợ giúp"} toggle={toggle} />
            <SidebarItem icon={<FeedbackIcon />} title={"Gửi phản hồi"} toggle={toggle} />
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

SidebarShow.propTypes = {
  active: PropTypes.bool,
};

export default SidebarShow;
