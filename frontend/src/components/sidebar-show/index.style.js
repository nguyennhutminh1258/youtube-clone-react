import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
  container: {
    position: "relative",
    width: "100%",
    height: "100%",
  },

  lockScreen: {
    display: "none",
    position: "fixed",
    width: "100%",
    height: "100%",
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: "rgba(3, 3, 3, 0.3)",
    zIndex: 2000,
    "&.toggle": {
      display: "block",
    },
  },
  sidebarContainer: {
    position: "fixed",
    top: 0,
    left: "-100%",
    width: "240px",
    height: "100%",
    overflow: "hidden",
    backgroundColor: "rgba(255, 255, 255, 0.98)",
    overscrollBehavior: "contain",
    zIndex: "2000",
    transition: "left 0.2s",
    "&:hover": {
      overflowY: "auto",
    },
    "&.toggle": {
      left: "0",
    },
  },
  sidebar: {
    width: "240px",
    paddingRight: "12px",
    overscrollBehavior: "contain",
  },

  sidebarHeader: {
    width: "240px",
    height: "56px",
    marginTop: "8px",
    paddingLeft: "15px",
  },

  menu: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    paddingRight: "35px",
  },

  iconBar: {
    height: "40px",
    width: "40px",
    padding: "8px",
    borderRadius: "50%",
    marginRight: "15px",
    fill: "rgb(13, 13, 13)",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#f0f0f0",
    },
  },

  logo: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "90px",
    height: "20px",
  },

  sidebarLists: {
    padding: "8px 0",
    borderBottom: "1px solid rgb(230, 220, 220)",
    "&:nth-child(1)": {
      padding: "12px 0",
    },
    "&:last-child": {
      borderBottom: "none",
    },
  },

  sidebarTitle: {
    color: "#606060",
    padding: "8px 24px",
    fontSize: "14px",
    fontWeight: "500",
    letterSpacing: "0.5px",
    textTransform: "uppercase",
  },
});
