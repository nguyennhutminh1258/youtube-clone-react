import { Skeleton } from "@mui/material";
import Box from "@mui/material/Box";
import Tippy from "@tippyjs/react";
import { gapi } from "gapi-script";
import PropTypes from "prop-types";
import { useEffect, useRef, useState } from "react";
import GoogleLogin from "react-google-login";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { followCursor } from "tippy.js";
import { ReactComponent as BtnBack } from "../../assets/images/btn-back.svg";
import { ReactComponent as ClearInput } from "../../assets/images/clear-input.svg";
import { ReactComponent as IconBar } from "../../assets/images/icon-bar.svg";
import { ReactComponent as IconMicro } from "../../assets/images/icon-micro.svg";
import { ReactComponent as IconOption } from "../../assets/images/icon-option.svg";
import { ReactComponent as IconSearch } from "../../assets/images/icon-search.svg";
import { ReactComponent as NotificationIcon } from "../../assets/images/notification-icon.svg";
import { ReactComponent as TopbarIcon } from "../../assets/images/topbar-icon.svg";
import { ReactComponent as UserIcon } from "../../assets/images/user-icon.svg";
import { ReactComponent as LogoYoutube } from "../../assets/images/youtube-logo.svg";
import { setShowInput, setSmallSidebar, setToggle } from "../../store/slice/uiSlice";
import { setUser } from "../../store/slice/userSlice";
import MenuDropdown from "../menu-dropdown";
import { useStyles } from "./index.style";

const Header = ({ detail }) => {
  const dispatch = useDispatch();
  const classes = useStyles();

  const smallSidebar = useSelector((state) => state.ui.smallSidebar);
  const toggle = useSelector((state) => state.ui.toggle);
  const showInput = useSelector((state) => state.ui.showInput);
  const width = useSelector((state) => state.ui.width);
  const user = useSelector((state) => state.user);
  const loadingVideo = useSelector((state) => state.video.loadingVideo);
  const numberVideo = useSelector((state) => state.video.numberVideo);

  const headerRef = useRef();
  const iconSearchRef = useRef();
  const inputSearchRef = useRef();
  const clearInputRef = useRef();

  const [input, setInput] = useState("");

  const userInfo =
    localStorage.getItem("user") !== "undefined"
      ? JSON.parse(localStorage.getItem("user"))
      : localStorage.clear;

  const responseGoogle = (response) => {
    localStorage.setItem("user", JSON.stringify(response.profileObj));
    const { name, googleId, imageUrl } = response.profileObj;

    const doc = {
      _id: googleId,
      _type: "user",
      userName: name,
      image: imageUrl,
    };
    dispatch(setUser(doc));
    window.location.reload();
  };

  const handleOnChange = (event) => {
    clearInputRef.current.style.display = "flex";
    setInput(event.target.value);
  };

  const handleClearInput = () => {
    setInput("");
    inputSearchRef.current.focus();
    clearInputRef.current.style.display = "none";
  };

  const handleShowSidebar = () => {
    if (width <= 1315) {
      dispatch(setToggle(!toggle));
    } else if (detail) {
      dispatch(setToggle(!toggle));
    } else {
      dispatch(setSmallSidebar(!smallSidebar));
    }
  };

  const handleSearchClick = () => {
    if (width <= 656) {
      dispatch(setShowInput(true));
    }
  };

  const handleCloseInput = () => {
    dispatch(setShowInput(false));
  };

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: process.env.REACT_APP_GOOGLE_API_TOKEN,
        scope: "email",
      });
    }

    gapi.load("client:auth2", start);
  }, []);

  useEffect(() => {
    if (input === "") {
      clearInputRef.current.style.display = "none";
    }
  }, [input]);

  useEffect(() => {
    if (width > 656) {
      dispatch(setShowInput(false));
    }
  }, [width, dispatch]);

  useEffect(() => {
    if (showInput) {
      inputSearchRef.current.focus();
    }
  }, [showInput]);

  if (loadingVideo && typeof numberVideo === "undefined") {
    return (
      <Box className={classes.header} ref={headerRef}>
        <Box className={classes.container}>
          <Box className={`${classes.left} ${showInput ? "showInput" : ""}`}>
            <Box className={classes.iconBar} onClick={() => handleShowSidebar()}>
              <IconBar />
            </Box>
          </Box>
          <Box className={`${classes.center} ${showInput ? "showInput" : ""}`}>
            <Box className={classes.search}></Box>
          </Box>
          <Box className={`${classes.right} ${showInput ? "showInput" : ""}`}>
            <Box>
              <button className={classes.btnSetting}>
                <Skeleton variant="circular" width={"35px"} height={"35px"}>
                  <TopbarIcon style={{ width: "24px", height: "24px" }} />
                </Skeleton>
              </button>
            </Box>
            <Box>
              <button className={classes.btnNotification}>
                <Skeleton variant="circular" width={"35px"} height={"35px"}>
                  <NotificationIcon style={{ width: "24px", height: "24px" }} />
                </Skeleton>
              </button>
            </Box>
            <Box>
              <Skeleton variant="circular" width={"35px"} height={"35px"}>
                <img src={userInfo?.imageUrl} alt="user" className={classes.imageUser} />
              </Skeleton>
            </Box>
          </Box>
        </Box>
      </Box>
    );
  }

  return (
    <Box className={classes.header} ref={headerRef}>
      <Box className={classes.container}>
        <Box className={`${classes.left} ${showInput ? "showInput" : ""}`}>
          <Box className={classes.iconBar} onClick={() => handleShowSidebar()}>
            <IconBar />
          </Box>
          <Box>
            <Tippy
              content="Trang chủ Youtube"
              className={classes.tooltipLogo}
              delay={[1000, 0]}
              maxWidth="none"
              arrow={false}
              offset={[10, 20]}
              placement="bottom-start"
              followCursor="initial"
              plugins={[followCursor]}>
              <Link to="/">
                <LogoYoutube className={classes.logo} />
              </Link>
            </Tippy>
          </Box>
          <Box className={classes.btnBackContainer}>
            <button className={classes.btnBack} onClick={() => handleCloseInput()}>
              <BtnBack />
            </button>
          </Box>
        </Box>
        <Box className={`${classes.center} ${showInput ? "showInput" : ""}`}>
          <Box className={classes.search}>
            <Box className={classes.searchContainer}>
              <Box className={classes.iconSearchContainer}>
                <IconSearch className={classes.iconSearch} ref={iconSearchRef} />
              </Box>
              <input
                className={classes.inputSearch}
                type="text"
                placeholder="Tìm kiếm"
                ref={inputSearchRef}
                value={input}
                onChange={(event) => handleOnChange(event)}
              />
              <Box className={classes.virtualKeyboard}>
                <img src="https://www.gstatic.com/inputtools/images/tia.png" alt="" />
              </Box>
              <Box className={classes.clearInputContainer} ref={clearInputRef}>
                <ClearInput className={classes.clearInput} onClick={() => handleClearInput()} />
              </Box>
            </Box>
            <Tippy
              content="Tìm kiếm"
              className={classes.tooltipBtn}
              maxWidth="none"
              placement="bottom"
              arrow={false}
              offset={[0, 10]}
              delay={[500]}>
              <button className={classes.btnSearch} onClick={() => handleSearchClick()}>
                <IconSearch style={{ width: "24px", height: "24px" }} />
              </button>
            </Tippy>
            <Tippy
              content="Tìm kiếm bằng giọng nói"
              className={classes.tooltipBtn}
              maxWidth="none"
              placement="bottom"
              arrow={false}
              offset={[0, 10]}
              delay={[500, 0]}>
              <button className={classes.btnMicro}>
                <IconMicro />
              </button>
            </Tippy>
          </Box>
        </Box>
        {Object.keys(user).length !== 0 || userInfo ? (
          <Box className={`${classes.right} ${showInput ? "showInput" : ""}`}>
            <Box>
              <Tippy
                content="Tạo"
                className={classes.tooltipBtn}
                maxWidth="none"
                placement="bottom"
                arrow={false}
                offset={[0, 10]}
                delay={[500, 0]}>
                <button className={classes.btnSetting}>
                  <TopbarIcon style={{ width: "24px", height: "24px" }} />
                </button>
              </Tippy>
            </Box>
            <Box>
              <Tippy
                content="Thông báo"
                className={classes.tooltipBtn}
                maxWidth="none"
                placement="bottom"
                arrow={false}
                offset={[0, 10]}
                delay={[500, 0]}>
                <button className={classes.btnNotification}>
                  <NotificationIcon style={{ width: "24px", height: "24px" }} />
                </button>
              </Tippy>
            </Box>
            <Tippy
              content={<MenuDropdown userInfo={userInfo} login={true} />}
              arrow={false}
              placement="bottom-end"
              trigger="click"
              interactive="true"
              maxWidth="none"
              allowHTML="true">
              <Box>
                <img src={userInfo?.imageUrl} alt="user" className={classes.imageUser} />
              </Box>
            </Tippy>
          </Box>
        ) : (
          <Box className={`${classes.right} ${showInput ? "showInput" : ""}`}>
            <Box>
              <Tippy
                content="Cài đặt"
                className={classes.tooltipBtn}
                maxWidth="none"
                placement="bottom"
                arrow={false}
                offset={[0, 10]}
                delay={[500, 0]}>
                <Tippy
                  content={<MenuDropdown userInfo={userInfo} login={false} />}
                  arrow={false}
                  placement="bottom-end"
                  trigger="click"
                  interactive="true"
                  maxWidth="none"
                  allowHTML="true">
                  <button className={classes.btnSetting}>
                    <IconOption className={classes.IconOption} />
                  </button>
                </Tippy>
              </Tippy>
            </Box>
            <Box>
              <GoogleLogin
                clientId={process.env.REACT_APP_GOOGLE_API_TOKEN}
                render={(renderProps) => (
                  <button
                    className={classes.btnLogin}
                    onClick={renderProps.onClick}
                    disabled={renderProps.disabled}>
                    <UserIcon className={classes.userIcon} />
                    Đăng nhập
                  </button>
                )}
                onSuccess={(response) => responseGoogle(response)}
                onFailure={(response) => responseGoogle(response)}
                cookiePolicy="single_host_origin"
              />
            </Box>
          </Box>
        )}
      </Box>
    </Box>
  );
};

Header.propTypes = {
  detail: PropTypes.bool,
};

export default Header;
