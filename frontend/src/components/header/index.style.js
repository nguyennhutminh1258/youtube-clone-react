import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
  header: {
    position: "sticky",
    width: "100%",
    backgroundColor: "rgba(255, 255, 255, 0.98)",
    top: "0",
    zIndex: "30",
  },

  container: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    height: "56px",
    padding: "0 16px",
    "@media (max-width: 656px)": {
      padding: "0 8px",
    },
  },

  left: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    "&.showInput": {
      "& $iconBar, $logo": {
        display: "none",
      },
      "& $btnBackContainer": {
        display: "block",
      },
    },
  },

  iconBar: {
    height: "40px",
    width: "40px",
    padding: "8px",
    borderRadius: "50%",
    marginRight: "15px",
    fill: "rgb(13, 13, 13)",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#f0f0f0",
    },
  },

  logo: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "90px",
    height: "20px",
  },

  tooltipLogo: {
    backgroundColor: "#3c3c3c",
    padding: "2px 6px",
    color: "#f0f0f0",
    fontWeight: 400,
    fontSize: "12px",
    lineHeight: "18px",
  },

  btnBackContainer: {
    display: "none",
    padding: "8px",
  },

  btnBack: {
    height: "24px",
    width: "24px",
    backgroundColor: "transparent",
    border: "none",
    cursor: "pointer",
  },

  center: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    flex: "0 1 728px",
    minWidth: "0px",
    "@media (max-width: 656px)": {
      flex: 1,
      justifyContent: "flex-end",
    },
    "&.showInput": {
      "& $search": {
        margin: 0,
      },
      "& $btnMicro": {
        display: "flex",
      },
      "& $searchContainer": {
        display: "flex",
      },
      "& $btnSearch": {
        border: "1px solid #d3d3d3",
        backgroundColor: "#f8f8f8",
        "&:hover": {
          backgroundColor: "#f0f0f0",
        },
      },
      "@media (max-width: 430px)": {
        "& $btnMicro": {
          display: "none",
        },
      },
    },
  },

  search: {
    display: "flex",
    flex: "1",
    margin: "0 0 0 40px",
    padding: "0 4px",
    "@media (max-width: 656px)": {
      justifyContent: "flex-end",
    },
    "@media (max-width: 560px)": {
      margin: "0",
      padding: "0",
    },
  },

  searchContainer: {
    display: "flex",
    flex: "1",
    flexDirection: "row",
    alignItems: "center",
    height: "40px",
    marginLeft: "34px",
    border: "1px solid #ccc",
    borderRight: "none",
    borderRadius: "2px 0 0 2px",
    "&:focus-within": {
      border: "1px solid #065fd4",
      marginLeft: "0px",
      "& $iconSearchContainer": {
        display: "block",
      },
      "& $inputSearch": {
        paddingLeft: "10px",
      },
    },
    "@media (max-width: 656px)": {
      display: "none",
    },
  },

  iconSearchContainer: {
    paddingLeft: "8px",
    display: "none",
  },

  iconSearch: {
    width: "20px",
    height: "20px",
  },

  inputSearch: {
    width: "100%",
    height: "100%",
    border: "none",
    outline: "none",
    fontSize: "16px",
    lineHeight: "24px",
    fontWeight: "400",
    padding: "2px 4px",
  },

  virtualKeyboard: {
    paddingRight: "8px",
    cursor: "pointer",
  },

  clearInputContainer: {
    alignItems: "center",
    justifyContent: "center",
    marginRight: "8px",
    display: "none",
    cursor: "pointer",
  },

  clearInput: {
    height: "24px",
    width: "24px",
  },

  btnSearch: {
    width: "64px",
    height: "40px",
    borderRadius: "0 2px 2px 0",
    border: "1px solid #d3d3d3",
    backgroundColor: "#f8f8f8",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#f0f0f0",
      "@media (max-width: 656px)": {
        background: "transparent",
      },
    },
    "@media (max-width: 656px)": {
      background: "transparent",
      border: "none",
    },
  },

  tooltipBtn: {
    backgroundColor: "#606060",
    padding: "8px",
    color: "#f0f0f0",
    fontWeight: 400,
    fontSize: "13px",
    lineHeight: "18px",
  },

  btnMicro: {
    backgroundColor: "#f9f9f9",
    marginLeft: "4px",
    height: "40px",
    width: "40px",
    borderRadius: "100px",
    border: "none",
    outline: "none",
    cursor: "pointer",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    "&:hover": {
      backgroundColor: "#f0f0f0",
    },
    "@media (max-width: 560px)": {
      display: "none",
    },
  },

  right: {
    minWidth: "225px",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    "@media (max-width: 656px)": {
      minWidth: "0px",
      flex: "none",
    },
    "&.showInput": {
      display: "none",
    },
  },

  btnSetting: {
    width: "40px",
    height: "40px",
    borderRadius: "50%",
    border: "none",
    backgroundColor: "transparent",
    marginRight: "8px",
    cursor: "pointer",
    "@media (max-width: 656px)": {
      marginRight: "0px",
    },
  },

  btnNotification: {
    width: "40px",
    height: "40px",
    borderRadius: "50%",
    border: "none",
    backgroundColor: "transparent",
    marginRight: "8px",
    cursor: "pointer",
    "@media (max-width: 656px)": {
      marginRight: "0px",
    },
    "@media (max-width: 560px)": {
      display: "none",
    },
  },

  btnLogin: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    gap: "8px",
    padding: "6px 12px",
    color: "#065fd4",
    border: "1px solid #065fd4",
    backgroundColor: "transparent",
    fontSize: "14px",
    fontWeight: "600",
    textTransform: "uppercase",
    cursor: "pointer",
    "@media (max-width: 1315px)": {
      padding: "5px 11px",
    },
  },

  userIcon: {
    height: "24px",
    width: "24px",
    fill: "#065fd4",
  },

  imageUser: {
    cursor: "pointer",
    height: "32px",
    width: "32px",
    borderRadius: "50%",
  },
});
