import Box from "@mui/material/Box";
import PropTypes from "prop-types";
import { useStyles } from "./index.style";

const MenuVideo = ({ detailVideo, detailOption }) => {
  const classes = useStyles();

  return (
    <Box className={`${classes.container} ${detailVideo ? "detail" : ""}`}>
      <Box className={classes.menu}>
        <Box className={classes.itemContainer}>
          <Box className={classes.item}>
            <Box className={classes.icon}>
              {detailOption ? (
                <svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" focusable="false">
                  <g>
                    <path d="M13.18,4l0.24,1.2L13.58,6h0.82H19v7h-5.18l-0.24-1.2L13.42,11H12.6H6V4H13.18 M14,3H5v18h1v-9h6.6l0.4,2h7V5h-5.6L14,3 L14,3z"></path>
                  </g>
                </svg>
              ) : (
                <svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" focusable="false">
                  <g>
                    <path d="M21,16h-7v-1h7V16z M21,11H9v1h12V11z M21,7H3v1h18V7z M10,15l-7-4v8L10,15z"></path>
                  </g>
                </svg>
              )}
            </Box>
            <Box className={classes.title}>{detailOption ? "Báo vi phạm" : "Thêm vào danh sách chờ"}</Box>
          </Box>
        </Box>
        <Box className={classes.itemContainer}>
          <Box className={classes.item}>
            <Box className={classes.icon}>
              {detailOption ? (
                <svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" focusable="false">
                  <g>
                    <path d="M5,11h2v2H5V11z M15,15H5v2h10V15z M19,15h-2v2h2V15z M19,11H9v2h10V11z M22,6H2v14h20V6z M3,7h18v12H3V7z"></path>
                  </g>
                </svg>
              ) : (
                <svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" focusable="false">
                  <g mirror-in-rtl="">
                    <path d="M15,5.63L20.66,12L15,18.37V15v-1h-1c-3.96,0-7.14,1-9.75,3.09c1.84-4.07,5.11-6.4,9.89-7.1L15,9.86V9V5.63 M14,3v6 C6.22,10.13,3.11,15.33,2,21c2.78-3.97,6.44-6,12-6v6l8-9L14,3L14,3z"></path>
                  </g>
                </svg>
              )}
            </Box>
            <Box className={classes.title}>{detailOption ? "Hiện bản chép lời" : "Chia sẻ"}</Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

MenuVideo.propTypes = {
  detailVideo: PropTypes.bool,
  detailOption: PropTypes.bool,
};

export default MenuVideo;
