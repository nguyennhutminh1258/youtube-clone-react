import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
  container: {
    backgroundColor: "rgba(255, 255, 255, 0.98)",
    borderRadius: "4px",
    boxShadow:
      "0 2px 2px 0 rgb(0 0 0 / 14%), 0 1px 5px 0 rgb(0 0 0 / 12%), 0 3px 1px -2px rgb(0 0 0 / 20%)",
    "&.detail": {
      "& $itemContainer:last-child": {
        display: "none",
      },
    },
  },
  menu: {
    padding: "8px 0",
    color: "#030303",
  },
  itemContainer: {
    "&:hover": {
      backgroundColor: "#f0f0f0",
    },
  },
  item: {
    padding: "0 12px 0 16px",
    fontSize: "14px",
    fontWeight: 400,
    lineHeight: "24px",
    minHeight: "36px",
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    cursor: "pointer",
  },
  icon: {
    marginRight: "16px",
    "& svg": {
      height: "24px",
      width: "24px",
    },
  },
  title: {
    marginRight: "24px",
  },
});
