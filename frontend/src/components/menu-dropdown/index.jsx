import Box from "@mui/material/Box";
import PropTypes from "prop-types";
import { GoogleLogout } from "react-google-login";
import { useDispatch } from "react-redux";
import { ReactComponent as ArrowIcon } from "../../assets/images/menu/arrow-icon.svg";
import { ReactComponent as ChangeAccount } from "../../assets/images/menu/change-account.svg";
import { ReactComponent as LanguageIcon } from "../../assets/images/menu/language-icon.svg";
import { ReactComponent as Location } from "../../assets/images/menu/location.svg";
import { ReactComponent as LogoutIcon } from "../../assets/images/menu/logout-icon.svg";
import { ReactComponent as MemberPackage } from "../../assets/images/menu/member-package.svg";
import { ReactComponent as MyChannel } from "../../assets/images/menu/my-channel.svg";
import { ReactComponent as MyData } from "../../assets/images/menu/my-data.svg";
import { ReactComponent as RestrictMode } from "../../assets/images/menu/restrict-mode.svg";
import { ReactComponent as ShortcutIcon } from "../../assets/images/menu/shortcut-icon.svg";
import { ReactComponent as UiTheme } from "../../assets/images/menu/ui-theme.svg";
import { ReactComponent as YoutubeStudio } from "../../assets/images/menu/youtube-studio.svg";
import { ReactComponent as FeedbackIcon } from "../../assets/images/sidebar/feedback-icon.svg";
import { ReactComponent as SettingIcon } from "../../assets/images/sidebar/setting-icon.svg";
import { ReactComponent as SupportIcon } from "../../assets/images/sidebar/support-icon.svg";
import { removeUser } from "../../store/slice/userSlice";
import { useStyles } from "./index.style";

const MenuDropdown = ({ userInfo, login }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const logout = () => {
    localStorage.clear();
    dispatch(removeUser());
    window.location.reload();
    // console.log(user);
    // console.log(userInfo);
  };
  return (
    <>
      {login ? (
        <Box className={classes.container}>
          <Box className={classes.menuDropdown}>
            <Box className={classes.menuListUserInfo}>
              <Box className={classes.menuItemUserInfo}>
                <Box className={classes.imageUserContainer}>
                  <img src={userInfo?.imageUrl} alt="user" className={classes.imageUser} />
                </Box>
                <Box className={classes.infoUser}>
                  <Box className={classes.userName} component="span">
                    {userInfo?.name}
                  </Box>
                  <Box className={classes.manageAccount} component="span">
                    Quản lý Tài khoản Google của bạn
                  </Box>
                </Box>
              </Box>
            </Box>
            <Box className={classes.menuList}>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <MyChannel />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Kênh của bạn
                </Box>
              </Box>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <YoutubeStudio />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  YouTube Studio
                </Box>
              </Box>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <ChangeAccount />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Chuyển đổi tài khoản
                </Box>
                <Box className="secondary-icon">
                  <ArrowIcon />
                </Box>
              </Box>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <LogoutIcon />
                </Box>
                <GoogleLogout
                  clientId={`${process.env.REACT_APP_GOOGLE_API_TOKEN}`}
                  render={(renderProps) => (
                    <Box
                      className={classes.menuTitle}
                      component="span"
                      onClick={renderProps.onClick}
                      disabled={renderProps.disabled}>
                      Đăng xuất
                    </Box>
                  )}
                  onLogoutSuccess={logout}
                  cookiePolicy="single_host_origin"
                />
              </Box>
            </Box>
            <Box className={classes.menuList}>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <MemberPackage />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Giao dịch mua và gói thành viên
                </Box>
              </Box>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <MyData />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Dữ liệu của bạn trong YouTube
                </Box>
              </Box>
            </Box>
            <Box className={classes.menuList}>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <UiTheme />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Giao diện: Sáng
                </Box>
                <Box className="secondary-icon">
                  <ArrowIcon />
                </Box>
              </Box>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <LanguageIcon />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Ngôn ngữ: Tiếng Việt
                </Box>
                <Box className="secondary-icon">
                  <ArrowIcon />
                </Box>
              </Box>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <RestrictMode />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Chế độ hạn chế: Đã tắt
                </Box>
                <Box className="secondary-icon">
                  <ArrowIcon />
                </Box>
              </Box>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <Location />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Địa điểm: Việt Nam
                </Box>
                <Box className="secondary-icon">
                  <ArrowIcon />
                </Box>
              </Box>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <ShortcutIcon />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Phím tắt
                </Box>
              </Box>
            </Box>
            <Box className={classes.menuList}>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <SettingIcon />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Cài đặt
                </Box>
              </Box>
            </Box>
            <Box className={classes.menuList}>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <SupportIcon />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Trợ giúp
                </Box>
              </Box>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <FeedbackIcon />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Gửi phản hồi
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
      ) : (
        <Box className={classes.container}>
          <Box className={classes.menuDropdown}>
            <Box className={classes.menuList}>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <MyData />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Dữ liệu của bạn trong YouTube
                </Box>
              </Box>
            </Box>
            <Box className={classes.menuList}>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <UiTheme />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Giao diện: Sáng
                </Box>
                <Box className="secondary-icon">
                  <ArrowIcon />
                </Box>
              </Box>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <LanguageIcon />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Ngôn ngữ: Tiếng Việt
                </Box>
                <Box className="secondary-icon">
                  <ArrowIcon />
                </Box>
              </Box>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <RestrictMode />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Chế độ hạn chế: Đã tắt
                </Box>
                <Box className="secondary-icon">
                  <ArrowIcon />
                </Box>
              </Box>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <Location />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Địa điểm: Việt Nam
                </Box>
                <Box className="secondary-icon">
                  <ArrowIcon />
                </Box>
              </Box>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <ShortcutIcon />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Phím tắt
                </Box>
              </Box>
            </Box>
            <Box className={classes.menuList}>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <SettingIcon />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Cài đặt
                </Box>
              </Box>
            </Box>
            <Box className={classes.menuList}>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <SupportIcon />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Trợ giúp
                </Box>
              </Box>
              <Box className={classes.menuItem}>
                <Box className={classes.menuIcon}>
                  <FeedbackIcon />
                </Box>
                <Box className={classes.menuTitle} component="span">
                  Gửi phản hồi
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
      )}
    </>
  );
};

MenuDropdown.propTypes = {
  userInfo: PropTypes.object,
  login: PropTypes.bool,
};

export default MenuDropdown;
