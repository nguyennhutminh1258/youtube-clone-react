import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
  container: {
    maxWidth: "300px",
    maxHeight: "428px",
    zIndex: 2030,
    "& svg": {
      height: "24px",
      width: "24px",
    },
  },

  menuDropdown: {
    width: "100%",
    border: "1px solid rgba(0, 0, 0, 0.1)",
    borderTop: "none",
    backgroundColor: "rgba(255, 255, 255, 0.98)",
    display: "flex",
    flexDirection: "column",
  },

  menuListUserInfo: {
    padding: "16px 0",
    "&:not(:last-child)": {
      borderBottom: "1px solid rgba(0, 0, 0, 0.1)",
    },
  },

  menuList: {
    padding: "8px 0",
    "&:not(:last-child)": {
      borderBottom: "1px solid rgba(0, 0, 0, 0.1)",
    },
  },

  menuItemUserInfo: {
    display: "flex",
    alignItems: "center",
    minHeight: "40px",
    height: "40px",
    padding: "0 16px",
  },

  menuItem: {
    display: "flex",
    alignItems: "center",
    minHeight: "40px",
    height: "40px",
    padding: "0 16px",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#f0f0f0",
    },
  },

  menuIcon: {
    marginRight: "16px",
  },

  menuTitle: {
    flex: 1,
    fontSize: "14px",
    fontWeight: 400,
    lineHeight: "20px",
  },

  imageUserContainer: {
    marginRight: "16px",
  },

  imageUser: {
    width: "40px",
    height: "40px",
    borderRadius: "50%",
  },

  infoUser: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
  },

  userName: {
    fontSize: "16px",
    lineHeight: "22px",
    fontWeight: 500,
    color: "#0f0f0f",
  },

  manageAccount: {
    marginTop: "8px",
    fontSize: "14px",
    lineHeight: "22px",
    fontWeight: 400,
    color: "#065fd4",
    cursor: "pointer",
  },
});
