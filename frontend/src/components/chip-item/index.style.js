import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
  container: {
    margin: "12px",
    marginLeft: "0",
    "&.detail": {
      margin: "8px",
      marginLeft: "0",
    },
    "&:first-child": {
      marginLeft: "24px",
      "&.detail": {
        marginLeft: "0",
      },
    },
    "&:last-child": {
      marginRight: "24px",
      "&.detail": {
        marginRight: "0",
      },
    },
  },
  chipItem: {
    wordWrap: "none",
    whiteSpace: "nowrap",
    borderRadius: "16px",
    border: "1px solid #ccc",
    backgroundColor: "rgba(0, 0, 0, 0.05)",
    height: "32px",
    padding: "0 12px",
    fontWeight: "400",
    fontSize: "14px",
    lineHeight: "20px",
    color: "#030303",
    transition: "background-color 0.5s cubic-bezier(0.05, 0, 0.1)",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#ddd",
    },
    "&.active": {
      backgroundColor: "#030303",
      color: "rgba(255, 255, 255, 0.98)",
      overflow: "hidden",
      borderColor: "#eee",
    },
  },
  tooltipChip: {
    backgroundColor: "#3c3c3c",
    padding: "2px 6px",
    color: "#f0f0f0",
    fontWeight: 400,
    fontSize: "12px",
    lineHeight: "18px",
    border: "1px solid rgba(255, 255, 255, 0.98)",
  },
});
