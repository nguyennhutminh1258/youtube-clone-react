import Box from "@mui/material/Box";
import Tippy from "@tippyjs/react";
import PropTypes from "prop-types";
import { followCursor } from "tippy.js";
import { useStyles } from "./index.style";

const ChipItem = ({ title, active, chipRef, detailChip }) => {
  const classes = useStyles();

  return (
    <Box className={`${classes.container} ${detailChip ? "detail" : ""}`} ref={chipRef}>
      <Tippy
        content={title}
        className={classes.tooltipChip}
        delay={[1000, 0]}
        maxWidth="none"
        arrow={false}
        offset={[10, 20]}
        placement="bottom-start"
        followCursor="initial"
        plugins={[followCursor]}>
        <button className={`${classes.chipItem} ${active ? "active" : ""}`}>{title}</button>
      </Tippy>
    </Box>
  );
};

ChipItem.propTypes = {
  title: PropTypes.string,
  active: PropTypes.bool,
  chipRef: PropTypes.object,
  detailChip: PropTypes.bool,
};

export default ChipItem;
