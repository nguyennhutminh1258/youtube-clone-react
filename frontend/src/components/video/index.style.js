import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
  container: {
    display: "flex",
    justifyContent: "center",
    width: "100%",
  },
  videoSection: {
    position: "relative",
    width: "100%",
    display: "flex",
    justifyContent: "center",
    flexWrap: "wrap",
    maxWidth: "calc(4 * 360px + 64px)",
    margin: "0 16px",
    "&.small": {
      maxWidth: "calc(5 * 360px + 64px)",
    },
  },
  rowContainer: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
  },
});
