import VirtualizedList from "@dwqs/react-virtual-list";
import Box from "@mui/material/Box";
import { memo, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import VideoItem from "../video-item";
import { useStyles } from "./index.style";

const VideoItemMemo = memo(VideoItem);

const Video = () => {
  const classes = useStyles();
  const [itemPerRow, setItemPerRow] = useState(4);

  const smallSidebar = useSelector((state) => state.ui.smallSidebar);
  const width = useSelector((state) => state.ui.width);
  const video = useSelector((state) => state.video.videoList);
  const hasMore = useSelector((state) => state.video.hasMore);

  const [rows, setRows] = useState([...Array(Math.ceil(video?.length / itemPerRow))]);
  const [videoRows, setVideoRows] = useState([]);

  useEffect(() => {
    setRows([...Array(Math.ceil(video?.length / itemPerRow))]);
    setVideoRows(
      rows?.map((row, index) => video?.slice(index * itemPerRow, index * itemPerRow + itemPerRow))
    );
  }, [itemPerRow, rows, video]);

  useEffect(() => {
    if (width <= 1150 && width > 875) {
      setItemPerRow(3);
    } else if (width <= 875 && width > 530) {
      setItemPerRow(2);
    } else if (width <= 530) {
      setItemPerRow(1);
    } else if (smallSidebar && width > 1690) {
      setItemPerRow(5);
    } else if (!smallSidebar && width < 1690) {
      setItemPerRow(4);
    }
  }, [width, smallSidebar]);

  return (
    <Box className={classes.container}>
      <Box className={`${classes.videoSection} ${smallSidebar && width > 1690 ? "small" : ""}`}>
        <VirtualizedList
          hasMore={hasMore}
          overscanCount={0}
          itemCount={videoRows?.length}
          renderItem={({ index }) => {
            const row = videoRows[index];
            return (
              <Box className={classes.rowContainer}>
                {row?.map((item) => {
                  const {
                    _id,
                    videoImage,
                    videoLength,
                    videoTitle,
                    videoView,
                    timePublic,
                    verifiedIcon,
                    channelImage,
                    channelName,
                  } = item;
                  return (
                    <VideoItemMemo
                      key={_id}
                      _id={_id}
                      videoImage={videoImage}
                      videoLength={videoLength}
                      videoTitle={videoTitle}
                      videoView={videoView}
                      timePublic={timePublic}
                      verifiedIcon={verifiedIcon}
                      channelImage={channelImage}
                      channelName={channelName}
                    />
                  );
                })}
              </Box>
            );
          }}
        />
      </Box>
    </Box>
  );
};

export default Video;
