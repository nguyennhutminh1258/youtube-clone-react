import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
  header: {
    marginTop: "24px",
    marginBottom: "32px",
    display: "flex",
    flexDirection: "column",
  },
  title: {
    marginBottom: "24px",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  commentCount: {
    marginRight: "32px",
    fontSize: "16px",
    lineHeight: "22px",
    fontWeight: "400",
    color: "#0f0f0f",
  },
  tooltipSortMenu: {
    backgroundColor: "#606060",
    padding: "8px",
    color: "#f0f0f0",
    fontWeight: 400,
    fontSize: "13px",
    lineHeight: "18px",
  },
  sortMenu: {
    color: "#606060",
    maxWidth: "100%",
    cursor: "pointer",
    fontSize: "16px",
    fontWeight: "400",
    lineHeight: "22px",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  sortIcon: {
    marginRight: "8px",
  },
  sortLabel: {
    color: "#0f0f0f",
    fontWeight: 500,
    fontSize: "14px",
    letterSpacing: "0.5px",
    textTransform: "uppercase",
  },
});
