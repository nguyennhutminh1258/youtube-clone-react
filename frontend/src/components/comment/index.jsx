import Box from "@mui/material/Box";
import Tippy from "@tippyjs/react";
import PropTypes from "prop-types";
import { memo } from "react";
import { ReactComponent as SortIcon } from "../../assets/images/sort-icon.svg";
import CommentItem from "../comment-item";
import CommentSelf from "../comment-self";
import { useStyles } from "./index.style";

const CommentItemMemo = memo(CommentItem);

const Comment = ({ comment }) => {
  const classes = useStyles();

  return (
    <Box className={classes.container}>
      <Box className={classes.header}>
        <Box className={classes.title}>
          <h2 className={classes.commentCount}>{comment?.length ? `${comment?.length}` : "0"} bình luận</h2>
          <Tippy
            className={classes.tooltipSortMenu}
            content="Sắp xếp bình luận"
            arrow={false}
            maxWidth="none"
            placement="bottom"
            offset={[0, 20]}
            duration={[500, 0]}>
            <Box className={classes.sortMenu}>
              <Box className={classes.sortIcon}>
                <SortIcon />
              </Box>
              <Box className={classes.sortLabel} component="span">
                Sắp xếp theo
              </Box>
            </Box>
          </Tippy>
        </Box>
        <Box className={classes.commentSelf}>
          <CommentSelf />
        </Box>
      </Box>
      <Box className={classes.content}>
        {comment?.map((item) => {
          const { _key, postedBy, publishedTimeText, content, likeCounter } = item;
          return (
            <CommentItemMemo
              key={_key}
              _key={_key}
              imageAuthor={postedBy?.image}
              authorName={postedBy?.userName}
              publishedTimeText={publishedTimeText}
              commentContent={content}
              likeCounter={likeCounter}
            />
          );
        })}
      </Box>
    </Box>
  );
};

Comment.propTypes = {
  comment: PropTypes.array,
};

export default Comment;
