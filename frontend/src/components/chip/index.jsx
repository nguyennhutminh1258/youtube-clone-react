import Box from "@mui/material/Box";
import PropTypes from "prop-types";
import { memo, useCallback, useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { ReactComponent as LeftArrow } from "../../assets/images/left-arrow.svg";
import { ReactComponent as RightArrow } from "../../assets/images/right-arrow.svg";
import { SmoothHorizontalScrolling } from "../../utils/scrollChips";
import ChipItem from "../chip-item";
import { useStyles } from "./index.style";

const ChipItemMemo = memo(ChipItem);

const Chip = ({ detail }) => {
  const classes = useStyles();

  const sliderRef = useRef();
  const chipRef = useRef();

  const [showLeftBtn, setShowLeftBtn] = useState(false);
  const [showRightBtn, setShowRightBtn] = useState(false);
  const [dragDown, setDragDown] = useState(0);
  const [dragMove, setDragMove] = useState(0);
  const [isDrag, setIsDrag] = useState(false);

  const loadingVideo = useSelector((state) => state.video.loadingVideo);
  const numberVideo = useSelector((state) => state.video.numberVideo);

  const handleScrollRight = () => {
    const maxScrollLeft = sliderRef.current.scrollWidth - sliderRef.current.clientWidth;
    if (sliderRef.current.scrollLeft < maxScrollLeft) {
      SmoothHorizontalScrolling(
        sliderRef.current,
        150,
        chipRef.current.clientWidth * 5,
        sliderRef.current.scrollLeft
      );
    }
  };

  const handleScrollLeft = () => {
    if (sliderRef.current.scrollLeft > 0) {
      SmoothHorizontalScrolling(
        sliderRef.current,
        120,
        -chipRef.current.clientWidth * 5,
        sliderRef.current.scrollLeft
      );
    }
  };

  const handleScroll = () => {
    const slider = sliderRef.current;
    setShowLeftBtn(slider.scrollLeft > 0);
    setShowRightBtn(slider.scrollWidth > slider.scrollLeft + slider.offsetWidth);
  };

  const onDragStart = (e) => {
    var img = new Image();
    img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAUEBAAAACwAAAAAAQABAAACAkQBADs=";
    e.dataTransfer.setDragImage(img, 0, 0);
    setIsDrag(true);
    setDragDown(e.screenX);
  };

  const onTouchStart = (e) => {
    setIsDrag(true);
    setDragDown(e.changedTouches[0].clientX);
  };

  const onDragEnter = (e) => {
    setDragMove(e.screenX);
  };

  const onTouchMove = (e) => {
    setDragMove(e.changedTouches[0].clientX);
  };

  const onDragEnd = () => {
    setIsDrag(false);
  };

  const onTouchEnd = () => {
    setIsDrag(false);
  };

  const handleResize = useCallback(() => {
    const slider = sliderRef.current;
    setShowLeftBtn(slider.scrollLeft > 0);
    setShowRightBtn(slider.scrollWidth > slider.scrollLeft + slider.offsetWidth);
  }, []);

  useEffect(() => {
    if (isDrag) {
      if (dragMove < dragDown) handleScrollRight();
      if (dragMove > dragDown) handleScrollLeft();
    }
  }, [dragDown, dragMove, isDrag]);

  useEffect(() => {
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, [handleResize]);

  useEffect(() => {
    handleResize();
  }, [handleResize]);

  if (loadingVideo && typeof numberVideo === "undefined") {
    return (
      <Box className={`${classes.container} ${detail ? "detail" : ""}`} draggable={true}>
        <Box className={classes.chipSection} ref={sliderRef} onScroll={() => handleScroll()}></Box>
      </Box>
    );
  }

  return (
    <Box className={`${classes.container} ${detail ? "detail" : ""}`}>
      {showLeftBtn && (
        <Box className={classes.leftArrow}>
          <Box className={classes.leftArrowBtn}>
            <button onClick={() => handleScrollLeft()}>
              <LeftArrow />
            </button>
          </Box>
        </Box>
      )}
      <Box
        className={classes.chipSection}
        ref={sliderRef}
        onScroll={() => handleScroll()}
        draggable={true}
        onDragStart={onDragStart}
        onDragEnter={onDragEnter}
        onDragEnd={onDragEnd}
        onTouchStart={onTouchStart}
        onTouchMove={onTouchMove}
        onTouchEnd={onTouchEnd}>
        <ChipItemMemo title={"Tất cả"} active={true} chipRef={chipRef} detailChip={detail} />
        <ChipItemMemo title={"Trực tiếp"} chipRef={chipRef} detailChip={detail} />
        <ChipItemMemo title={"Trò chơi"} chipRef={chipRef} detailChip={detail} />
        <ChipItemMemo title={"Danh sách kết hợp"} chipRef={chipRef} detailChip={detail} />
        <ChipItemMemo title={"Âm nhạc"} chipRef={chipRef} detailChip={detail} />
        <ChipItemMemo title={"Bóng đá"} chipRef={chipRef} detailChip={detail} />
        <ChipItemMemo title={"Nấu ăn"} chipRef={chipRef} detailChip={detail} />
        <ChipItemMemo title={"Mới tải lên gần đây"} chipRef={chipRef} detailChip={detail} />
        <ChipItemMemo title={"Đề xuất mới"} chipRef={chipRef} detailChip={detail} />
        <ChipItemMemo title={"Tin tức"} chipRef={chipRef} detailChip={detail} />
      </Box>
      {showRightBtn && (
        <Box className={classes.rightArrow}>
          <Box className={classes.rightArrowBtn}>
            <button onClick={() => handleScrollRight()}>
              <RightArrow />
            </button>
          </Box>
        </Box>
      )}
    </Box>
  );
};

Chip.propTypes = {
  detail: PropTypes.bool,
};

export default Chip;
