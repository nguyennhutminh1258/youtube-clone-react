import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
  container: {
    display: "flex",
    justifyContent: "center",
    backgroundColor: "rgba(255, 255, 255, 0.98)",
    borderTop: "1px solid rgba(0, 0, 0, 0.1)",
    borderBottom: "1px solid rgba(0, 0, 0, 0.1)",
    position: "sticky",
    top: "56px",
    zIndex: "2",
    "&.detail": {
      position: "relative",
      top: 0,
      border: "none",
      backgroundColor: "#f9f9f9",
    },
  },
  chipSection: {
    position: "relative",
    display: "flex",
    alignItems: "center",
    margin: "0 auto",
    overflow: "hidden",
    width: "100%",
    maxWidth: "1200px",
    height: "56px",
    touchAction: "pan-y",
  },
  leftArrow: {
    position: "absolute",
    height: "100%",
    zIndex: "2020",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    left: 0,
    "&:after": {
      height: "100%",
      width: "50px",
      content: '""',
      pointerEvents: "none",
      background: "linear-gradient(to right,#fff 20%,rgba(255,255,255,0) 80%)",
    },
  },

  rightArrow: {
    position: "absolute",
    height: "100%",
    zIndex: "2020",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    right: 0,
    "&:before": {
      height: "100%",
      width: "50px",
      content: '""',
      pointerEvents: "none",
      background: "linear-gradient(to left, #fff 20%,rgba(255,255,255,0) 80%)",
    },
  },

  leftArrowBtn: {
    backgroundColor: "rgba(255, 255, 255, 0.98)",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    "& button": {
      outline: "none",
      border: "none",
      backgroundColor: "transparent",
      cursor: "pointer",
    },
  },

  rightArrowBtn: {
    backgroundColor: "rgba(255, 255, 255, 0.98)",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    "& button": {
      outline: "none",
      border: "none",
      backgroundColor: "transparent",
      cursor: "pointer",
    },
  },
});
