import { Skeleton } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import { useSelector } from "react-redux";
import { useStyles } from "../video-item/index.style";

const SkeletonLoader = ({ detailVideo }) => {
  const classes = useStyles();
  const width = useSelector((state) => state.ui.width);
  const smallSidebar = useSelector((state) => state.ui.smallSidebar);

  return (
    <Box
      className={`${classes.container} ${smallSidebar && width > 1690 ? "small" : ""} ${
        detailVideo ? "detail" : ""
      }`}>
      <Box className={classes.videoImage}>
        <Skeleton variant="rectangle" width={"100%"}>
          <img
            src={
              "https://i.ytimg.com/vi/jraNLkmnjto/hq720.jpg?sqp=-oaymwEcCOgCEMoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLAwAKfB5lBlQWEIYpjDFS9FtvrY6Q"
            }
            alt="video"
          />
        </Skeleton>
      </Box>
      <Box className={classes.videoDetail}>
        <Box className={classes.channelImage}>
          <Skeleton variant="circular" height={"40px"} width={"40px"}>
            <img
              src={
                "https://yt3.ggpht.com/ytc/AMLnZu_cLEQs_9Huv6ay0GzbRDXbB6eFjNRdxTkUelMl=s68-c-k-c0x00ffffff-no-rj"
              }
              alt="channelImage"
            />
          </Skeleton>
        </Box>
        <Box className={classes.videoInfo}>
          <Skeleton variant="rectangle" height={"20px"} width={"100%"}>
            <Box className={classes.videoTitle} component="h3">
              Trọn bộ 9 bài đỉnh nhất của Top 3 ai nghe qua cũng phải thích | The Masked Singer Vietnam
            </Box>
          </Skeleton>
          <Skeleton variant="rectangle" height={"20px"} width={"60%"} style={{ marginTop: "5px" }}>
            <Box className={classes.channelInfo}>
              <Box className={classes.channelName} component="span"></Box>
            </Box>
            <Box className={classes.videoMoreInfo}>
              <Box className={classes.videoView} component="span"></Box>
              <Box className={classes.videoTimePublic} component="span"></Box>
            </Box>
          </Skeleton>
        </Box>
      </Box>
    </Box>
  );
};

export default SkeletonLoader;
