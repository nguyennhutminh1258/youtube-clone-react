import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
  tooltipSidebar: {
    backgroundColor: "#3c3c3c",
    padding: "2px 6px",
    color: "#f0f0f0",
    fontWeight: 400,
    fontSize: "12px",
    lineHeight: "18px",
  },

  sidebarItem: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    gap: "25px",
    padding: "6px 24px",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#f0f0f0",
    },
    "&.active": {
      backgroundColor: "rgba(0, 0, 0, 0.1)",
      fontWeight: "600",
    },
    "&.active:hover": {
      backgroundColor: "#c6c6c6",
    },
    "&.small": {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center",
      padding: "12px 0px 12px",
      gap: "0px",
      "&.active": {
        backgroundColor: "transparent",
      },
      "&.active:hover": {
        backgroundColor: "#f0f0f0",
      },
      "& $sidebarTitle": {
        fontSize: "10px",
      },
    },
    "@media (max-width: 1315px)": {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center",
      padding: "12px 0px 12px",
      gap: "0px",
      "&.active": {
        backgroundColor: "transparent",
      },
      "&.active:hover": {
        backgroundColor: "#f0f0f0",
      },
      "&.toggle": {
        flexDirection: "row",
        alignItems: "center",
        width: "100%",
        gap: "25px",
        padding: "6px 24px",
        "&.active": {
          backgroundColor: "rgba(0, 0, 0, 0.1)",
          fontWeight: "600",
        },
        "& $sidebarTitle": {
          fontSize: "14px",
        },
      },
    },
  },

  sidebarIcon: {
    height: "24px",
    width: "24px",
    "& img": {
      height: "24px",
      width: "24px",
      borderRadius: "50%",
    },
  },

  sidebarTitle: {
    flex: "1",
    fontWeight: "400",
    lineHeight: "20px",
    fontSize: "14px",
    "@media (max-width: 1315px)": {
      fontSize: "10px",
    },
  },

  sidebarLogin: {
    padding: "8px 32px",
  },

  text: {
    width: "200px",
    fontSize: "14px",
    color: "#030303",
    lineHeight: "20px",
    fontWeight: "400",
    wordWrap: "break-word",
  },

  btnLogin: {
    marginTop: "12px",
    padding: "5px 15px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    gap: "8px",
    color: "#065fd4",
    border: "1px solid #065fd4",
    backgroundColor: "transparent",
    fontSize: "14px",
    fontWeight: "600",
    textTransform: "uppercase",
    cursor: "pointer",
  },

  userIcon: {
    height: "24px",
    width: "24px",
    fill: "#065fd4",
  },
});
