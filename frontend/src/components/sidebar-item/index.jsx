import Box from "@mui/material/Box";
import Tippy from "@tippyjs/react";
import { gapi } from "gapi-script";
import PropTypes from "prop-types";
import { useEffect } from "react";
import GoogleLogin from "react-google-login";
import { useDispatch } from "react-redux";
import { followCursor } from "tippy.js";
import { ReactComponent as UserIcon } from "../../assets/images/user-icon.svg";
import { setUser } from "../../store/slice/userSlice";
import { useStyles } from "./index.style";

const SidebarItem = ({ icon, title, active, loginSection, small, toggle }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const responseGoogle = (response) => {
    localStorage.setItem("user", JSON.stringify(response.profileObj));
    const { name, googleId, imageUrl } = response.profileObj;

    const doc = {
      _id: googleId,
      _type: "user",
      userName: name,
      image: imageUrl,
    };
    dispatch(setUser(doc));
    window.location.reload();
  };

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: process.env.REACT_APP_GOOGLE_API_TOKEN,
        scope: "email",
      });
    }

    gapi.load("client:auth2", start);
  }, []);

  return (
    <>
      {loginSection ? (
        <Box className={classes.sidebarLogin}>
          <Box className={classes.text} component="span">
            Hãy đăng nhập để thích youtube, bình luận và đăng ký kênh.
          </Box>
          <GoogleLogin
            clientId={process.env.REACT_APP_GOOGLE_API_TOKEN}
            render={(renderProps) => (
              <button
                className={classes.btnLogin}
                onClick={renderProps.onClick}
                disabled={renderProps.disabled}>
                <UserIcon className={classes.userIcon} />
                Đăng nhập
              </button>
            )}
            onSuccess={(response) => responseGoogle(response)}
            onFailure={(response) => responseGoogle(response)}
            cookiePolicy="single_host_origin"
          />
        </Box>
      ) : small ? (
        <Tippy
          className={classes.tooltipSidebar}
          content={title}
          delay={[1000, 0]}
          arrow={false}
          maxWidth="none"
          offset={[10, 20]}
          placement="bottom-start"
          followCursor="initial"
          plugins={[followCursor]}>
          <Box className={`${classes.sidebarItem} ${active ? "active small" : "small"}`}>
            <Box className={classes.sidebarIcon}>{icon}</Box>
            <Box className={classes.sidebarTitle} component="span">
              {title}
            </Box>
          </Box>
        </Tippy>
      ) : toggle ? (
        <Tippy
          className={classes.tooltipSidebar}
          content={title}
          delay={[1000, 0]}
          maxWidth="none"
          arrow={false}
          offset={[10, 20]}
          placement="bottom-start"
          followCursor="initial"
          plugins={[followCursor]}>
          <Box className={`${classes.sidebarItem} ${active ? "active toggle" : "toggle"}`}>
            <Box className={classes.sidebarIcon}>{icon}</Box>
            <Box className={classes.sidebarTitle} component="span">
              {title}
            </Box>
          </Box>
        </Tippy>
      ) : (
        <Tippy
          className={classes.tooltipSidebar}
          content={title}
          delay={[1000, 0]}
          maxWidth="none"
          offset={[10, 20]}
          arrow={false}
          placement="bottom-start"
          followCursor="initial"
          plugins={[followCursor]}>
          <Box className={`${classes.sidebarItem} ${active ? "active" : ""}`}>
            <Box className={classes.sidebarIcon}>{icon}</Box>
            <Box className={classes.sidebarTitle} component="span">
              {title}
            </Box>
          </Box>
        </Tippy>
      )}
    </>
  );
};

SidebarItem.propTypes = {
  icon: PropTypes.object,
  title: PropTypes.string,
  active: PropTypes.bool,
  loginSection: PropTypes.bool,
  small: PropTypes.bool,
  toggle: PropTypes.bool,
};

export default SidebarItem;
