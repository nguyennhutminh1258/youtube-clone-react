import Box from "@mui/material/Box";
import Tippy from "@tippyjs/react";
import PropTypes from "prop-types";
import { useEffect, useRef, useState } from "react";
import { ReactComponent as OperationIcon } from "../../assets/images/detail/operation-icon.svg";
import { ReactComponent as SaveIcon } from "../../assets/images/detail/save-icon.svg";
import { ReactComponent as ShareIcon } from "../../assets/images/detail/share-icon.svg";
import { ReactComponent as ThumbsDown } from "../../assets/images/detail/thumbs-down.svg";
import { ReactComponent as ThumbsUp } from "../../assets/images/detail/thumbs-up.svg";
import { ReactComponent as VerifiedIconMusic } from "../../assets/images/video/verified-icon-music.svg";
import { ReactComponent as VerifiedIcon } from "../../assets/images/video/verified-icon.svg";
import MenuVideo from "../menu-video";
import { useStyles } from "./index.style";

const VideoInfo = ({
  videoTitle,
  videoView,
  dayPublic,
  verifiedIcon,
  channelImage,
  channelName,
  subscriber,
  likeCounter,
  description,
}) => {
  const classes = useStyles();
  const descriptionRef = useRef();
  const expandBtnRef = useRef();
  const hideBtnRef = useRef();
  const [isShowBtnDescription, setIsShowBtnDescription] = useState(false);

  const handleExpandDescription = () => {
    descriptionRef.current.style.WebkitLineClamp = "unset";
    expandBtnRef.current.style.display = "none";
    hideBtnRef.current.style.display = "block";
  };

  const handleHideDescription = () => {
    descriptionRef.current.style.WebkitLineClamp = "3";
    expandBtnRef.current.style.display = "block";
    hideBtnRef.current.style.display = "none";
  };

  const isEllipsisDescription = () => {
    setIsShowBtnDescription(
      descriptionRef.current.scrollHeight > descriptionRef.current.clientHeight ||
        descriptionRef.current.clientHeight > 60
    );
  };

  useEffect(() => {
    isEllipsisDescription();
  });

  return (
    <Box className={classes.container}>
      <Box className={classes.topRowContainer}>
        <Box className={classes.topRow}>
          <Box className={classes.titleContainer}>
            <h1 className={classes.title}>{videoTitle}</h1>
          </Box>
          <Box className={classes.infoVideo}>
            <Box className={classes.infoContainer}>
              <Box className={classes.info}>
                <span className={classes.view}>{videoView}</span>
                <span>{dayPublic}</span>
              </Box>
            </Box>
            <Box className={classes.actions}>
              <Box className={classes.actionsInner}>
                <Box className={classes.menu}>
                  <Tippy
                    className={classes.tooltipMenu}
                    content="Tôi thích video này"
                    arrow={false}
                    maxWidth="none"
                    offset={[30, 10]}
                    placement="bottom"
                    duration={[500, 0]}>
                    <Box className={classes.menuItem}>
                      <Box className={classes.icon}>
                        <ThumbsUp />
                      </Box>
                      <span className={classes.text}>{likeCounter}</span>
                    </Box>
                  </Tippy>
                  <Tippy
                    className={classes.tooltipMenu}
                    content="Tôi không thích video này"
                    arrow={false}
                    maxWidth="none"
                    placement="bottom"
                    duration={[500, 0]}>
                    <Box className={classes.menuItem}>
                      <Box className={classes.icon}>
                        <ThumbsDown />
                      </Box>
                      <span className={classes.text}>Không thích</span>
                    </Box>
                  </Tippy>
                  <Tippy
                    className={classes.tooltipMenu}
                    content="Chia sẻ"
                    arrow={false}
                    maxWidth="none"
                    placement="bottom"
                    duration={[500, 0]}>
                    <Box className={classes.menuItem}>
                      <Box className={classes.icon}>
                        <ShareIcon />
                      </Box>
                      <span className={classes.text}>Chia sẻ</span>
                    </Box>
                  </Tippy>
                  <Tippy
                    className={classes.tooltipMenu}
                    content="Lưu"
                    arrow={false}
                    maxWidth="none"
                    placement="bottom"
                    duration={[500, 0]}>
                    <Box className={classes.menuItem}>
                      <Box className={classes.icon}>
                        <SaveIcon />
                      </Box>
                      <span className={classes.text}>Lưu</span>
                    </Box>
                  </Tippy>
                  <Box className={classes.menuItem}>
                    <Box className={classes.icon}>
                      <Tippy
                        content={<MenuVideo detailOption={true} />}
                        arrow={false}
                        placement="bottom-start"
                        trigger="click"
                        interactive="true"
                        allowHTML="true">
                        <Box>
                          <OperationIcon />
                        </Box>
                      </Tippy>
                    </Box>
                  </Box>
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
      <Box className={classes.bottomRowContainer}>
        <Box className={classes.bottomRow}>
          <Box className={classes.owner}>
            <Box className={classes.videoOwner}>
              <Box className={classes.channelImgContainer}>
                <img src={channelImage} alt="channel_image" />
              </Box>
              <Box className={classes.channelInfo}>
                <Tippy
                  className={classes.tooltipMenu}
                  content={channelName}
                  arrow={false}
                  maxWidth="none"
                  placement="top"
                  offset={[-10, 20]}
                  duration={[500, 0]}>
                  <Box className={classes.channelNameContainer}>
                    <Box className={classes.channelName}>
                      <span>{channelName}</span>
                    </Box>
                    <Box className={classes.verifiedIcon}>
                      {verifiedIcon === "VerifiedIconMusic" ? (
                        <VerifiedIconMusic />
                      ) : verifiedIcon === "VerifiedIcon" ? (
                        <VerifiedIcon />
                      ) : (
                        ""
                      )}
                    </Box>
                  </Box>
                </Tippy>
                <Box className={classes.subCount}>
                  <span>{subscriber} người đăng ký</span>
                </Box>
              </Box>
            </Box>
            <Box className={classes.btnContainer}>
              {/* <button className={classes.sponsorBtn}>Tham gia</button> */}
              <button className={classes.subscribeBtn}>Đăng ký</button>
            </Box>
          </Box>
          <Box className={classes.description}>
            <Box className={classes.descriptionInner}>
              <Box className={classes.descriptionText}>
                <Box className={classes.textInlineContainer}>
                  <span className={classes.textInline} ref={descriptionRef}>
                    {description}
                  </span>
                </Box>
                {isShowBtnDescription && (
                  <>
                    <span
                      className={classes.expandBtn}
                      onClick={() => handleExpandDescription()}
                      ref={expandBtnRef}>
                      Hiện thêm
                    </span>
                    <span
                      className={classes.hideBtn}
                      onClick={() => handleHideDescription()}
                      ref={hideBtnRef}>
                      Ẩn bớt
                    </span>
                  </>
                )}
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

VideoInfo.propTypes = {
  videoTitle: PropTypes.string,
  videoView: PropTypes.string,
  dayPublic: PropTypes.string,
  verifiedIcon: PropTypes.string,
  channelImage: PropTypes.string,
  channelName: PropTypes.string,
  subscriber: PropTypes.string,
  likeCounter: PropTypes.string,
  description: PropTypes.string,
};

export default VideoInfo;
