import VirtualizedList from "@dwqs/react-virtual-list";
import Box from "@mui/material/Box";
import PropTypes from "prop-types";
import { memo } from "react";
import { useSelector } from "react-redux";
import Chip from "../chip";
import VideoItem from "../video-item";
import { useStyles } from "./index.style";

const VideoItemMemo = memo(VideoItem);

const VideoRecommend = ({ _id }) => {
  const classes = useStyles;
  const videoRecommend = useSelector((state) => state.video.videoList.filter((item) => item._id !== _id));
  const hasMore = useSelector((state) => state.video.hasMore);

  return (
    <Box className={classes.container}>
      <Chip detail={true} />
      <Box className={classes.content}>
        <VirtualizedList
          hasMore={hasMore}
          overscanCount={3}
          itemCount={videoRecommend?.length}
          renderItem={({ index }) => {
            const video = videoRecommend[index];
            const {
              _id,
              videoImage,
              videoLength,
              videoTitle,
              videoView,
              timePublic,
              verifiedIcon,
              channelImage,
              channelName,
            } = video;
            return (
              <VideoItemMemo
                key={_id}
                _id={_id}
                videoImage={videoImage}
                videoLength={videoLength}
                videoTitle={videoTitle}
                videoView={videoView}
                timePublic={timePublic}
                verifiedIcon={verifiedIcon}
                channelImage={channelImage}
                channelName={channelName}
                detailVideo={true}
              />
            );
          }}
        />
      </Box>
    </Box>
  );
};

VideoRecommend.propTypes = {
  _id: PropTypes.string,
};

export default VideoRecommend;
