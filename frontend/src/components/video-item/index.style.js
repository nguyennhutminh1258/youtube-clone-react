import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
  container: {
    display: "flex",
    flexDirection: "column",
    marginBottom: "40px",
    marginLeft: "8px",
    marginRight: "8px",
    width: "calc(100% / 4 - 16px)",
    backgroundColor: "#f9f9f9",
    "&:hover": {
      "& $iconOption": {
        visibility: "visible",
      },
    },
    "&.detail": {
      flexDirection: "row",
      width: "100%",
      marginTop: "8px",
      marginBottom: "0px",
      marginLeft: "0px",
      marginRight: "0px",
      "& $videoImage": {
        marginRight: "8px",
        "& img": {
          width: "168px",
          height: "94px",
        },
      },
      "& $channelImage": {
        display: "none",
      },
      "& $videoInfo": {
        paddingRight: 0,
      },
      "& $videoTitle": {
        marginTop: 0,
        fontSize: "14px",
      },
      "& $iconOption": {
        marginTop: 0,
      },
      "& $channelName": {
        fontSize: "12px",
      },
      "& $videoMoreInfo": {
        fontSize: "12px",
      },
    },
    "&.small": {
      width: "calc(100% / 5 - 16px)",
    },
    "@media (max-width: 1150px)": {
      width: "calc(100% / 3 - 16px)",
    },
    "@media (max-width: 875px)": {
      width: "calc(100% / 2 - 16px)",
    },
    "@media (max-width: 530px)": {
      width: "calc(100% - 16px)",
    },
  },
  videoImage: {
    position: "relative",
    cursor: "pointer",
    "& img": {
      width: "100%",
      height: "auto",
      backgroundColor: "#aaa",
    },
  },
  videoLength: {
    position: "absolute",
    bottom: "4px",
    right: "0px",
    color: "#fff",
    backgroundColor: "rgba(0, 0, 0, 0.8)",
    padding: "3px 4px",
    fontSize: "12px",
    fontWeight: "500",
    lineHeight: "12px",
    letterSpacing: "0.5px",
    display: "flex",
    alignItems: "center",
    margin: "4px",
    borderRadius: "2px",
  },
  videoDetail: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
  },
  channelImage: {
    height: "fit-content",
    marginTop: "12px",
    marginRight: "12px",
    cursor: "pointer",
    "& img": {
      width: "36px",
      height: "36px",
      borderRadius: "50%",
      backgroundColor: "#aaa",
    },
  },
  videoInfo: {
    paddingRight: "24px",
    "@media (max-width: 1570px)": {
      paddingRight: "0px",
    },
  },
  videoTitle: {
    fontSize: "16px",
    textOverflow: "ellipsis",
    overflow: "hidden",
    display: "-webkit-box",
    "-webkit-line-clamp": 2,
    "-webkit-box-orient": "vertical",
    whiteSpace: "normal",
    fontWeight: "500",
    lineHeight: "22px",
    color: "#030303",
    margin: "12px 0 4px 0",
    cursor: "pointer",
    "@media (max-width: 1570px)": {
      fontSize: "14px",
      lineHeight: "20px",
    },
  },
  tooltipVideoTitle: {
    backgroundColor: "#3c3c3c",
    padding: "2px 6px",
    color: "#f0f0f0",
    fontWeight: 400,
    fontSize: "12px",
    lineHeight: "18px",
  },
  channelInfo: {
    display: "flex",
  },
  channelName: {
    color: "#838282",
    overflow: "hidden",
    textOverflow: "ellipsis",
    display: "-webkit-box !important",
    "-webkit-line-clamp": 1,
    "-webkit-box-orient": "vertical",
    fontSize: "14px",
    lineHeight: "18px",
    fontWeight: "400",
    cursor: "pointer",
    "&:hover": {
      color: "#030303",
    },
    "@media (max-width: 1570px)": {
      fontSize: "12px",
    },
  },
  tooltipChannelName: {
    backgroundColor: "#606060",
    padding: "8px",
    color: "#f0f0f0",
    fontWeight: 400,
    fontSize: "13px",
    lineHeight: "18px",
  },
  verifiedIcon: {
    paddingLeft: "4px",
    fill: "#606060",
    height: "16px",
    width: "16px",
    "@media (max-width: 1315px)": {
      width: "14px",
      height: "14px",
    },
  },
  videoMoreInfo: {
    display: "flex",
    flexWrap: "wrap",
    fontSize: "14px",
    lineHeight: "18px",
    fontWeight: "400",
    color: "#606060",
    "@media (max-width: 1570px)": {
      fontSize: "12px",
    },
  },
  videoView: {
    "&:after": {
      content: '"•"',
      margin: "0 4px",
    },
  },
  iconOption: {
    marginTop: "12px",
    visibility: "hidden",
    cursor: "pointer",
    marginLeft: "auto",
  },
});
