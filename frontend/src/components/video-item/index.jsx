import Box from "@mui/material/Box";
import Tippy from "@tippyjs/react";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { followCursor } from "tippy.js";
import { ReactComponent as IconOption } from "../../assets/images/video/icon-option.svg";
import { ReactComponent as VerifiedIconMusic } from "../../assets/images/video/verified-icon-music.svg";
import { ReactComponent as VerifiedIcon } from "../../assets/images/video/verified-icon.svg";
import MenuVideo from "../menu-video";
import { useStyles } from "./index.style";

const VideoItem = ({
  _id,
  videoImage,
  videoLength,
  videoTitle,
  videoView,
  timePublic,
  verifiedIcon,
  channelImage,
  channelName,
  detailVideo,
}) => {
  const classes = useStyles();
  const width = useSelector((state) => state.ui.width);
  const smallSidebar = useSelector((state) => state.ui.smallSidebar);

  return (
    <Box
      className={`${classes.container} ${smallSidebar && width > 1690 ? "small" : ""} ${
        detailVideo ? "detail" : ""
      }`}>
      <Box className={classes.videoImage}>
        <Link to={`/detail/${_id}`} style={{ textDecoration: "none" }}>
          <img src={videoImage} alt="video" />
        </Link>
        <Box className={classes.videoLength} component="span">
          {videoLength}
        </Box>
      </Box>
      <Box className={classes.videoDetail}>
        <Tippy
          className={classes.tooltipVideoTitle}
          content={channelName}
          delay={[1000, 0]}
          maxWidth="none"
          arrow={false}
          offset={[10, 20]}
          placement="bottom-start"
          followCursor="initial"
          plugins={[followCursor]}>
          <Box className={classes.channelImage}>
            <img src={channelImage} alt="channelImage" />
          </Box>
        </Tippy>
        <Box className={classes.videoInfo}>
          <Link to={`/detail/${_id}`} style={{ textDecoration: "none" }}>
            <Tippy
              className={classes.tooltipVideoTitle}
              content={videoTitle}
              delay={[1000, 0]}
              arrow={false}
              maxWidth="none"
              offset={[10, 20]}
              placement="bottom-start"
              followCursor="initial"
              plugins={[followCursor]}>
              <Box className={classes.videoTitle} component="h3">
                {videoTitle}
              </Box>
            </Tippy>
          </Link>
          <Box className={classes.channelInfo}>
            <Tippy
              className={classes.tooltipChannelName}
              content={channelName}
              arrow={false}
              maxWidth="none"
              placement="top"
              delay={[500, 0]}
              duration={[500, 0]}>
              <Box className={classes.channelName} component="span">
                {channelName}
              </Box>
            </Tippy>
            <Box className={classes.verifiedIcon}>
              {verifiedIcon === "VerifiedIconMusic" ? (
                <VerifiedIconMusic />
              ) : verifiedIcon === "VerifiedIcon" ? (
                <VerifiedIcon />
              ) : (
                ""
              )}
            </Box>
          </Box>
          <Box className={classes.videoMoreInfo}>
            <Box className={classes.videoView} component="span">
              {videoView}
            </Box>
            <Box className={classes.videoTimePublic} component="span">
              {timePublic}
            </Box>
          </Box>
        </Box>
        <Box className={classes.iconOption}>
          <Tippy
            content={<MenuVideo detailVideo={detailVideo} />}
            arrow={false}
            placement="bottom-start"
            trigger="click"
            interactive="true"
            allowHTML="true">
            <Box>
              <IconOption />
            </Box>
          </Tippy>
        </Box>
      </Box>
    </Box>
  );
};

VideoItem.propTypes = {
  _id: PropTypes.string,
  videoImage: PropTypes.string,
  videoLength: PropTypes.string,
  videoTitle: PropTypes.string,
  videoView: PropTypes.string,
  timePublic: PropTypes.string,
  verifiedIcon: PropTypes.string,
  channelImage: PropTypes.string,
  channelName: PropTypes.string,
  detailVideo: PropTypes.bool,
};

export default VideoItem;
