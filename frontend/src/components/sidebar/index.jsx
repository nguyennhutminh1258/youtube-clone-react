import Box from "@mui/material/Box";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { ReactComponent as DiaryIcon } from "../../assets/images/sidebar/diary-icon.svg";
import { ReactComponent as ExploreIcon } from "../../assets/images/sidebar/explore-icon.svg";
import { ReactComponent as FeedbackIcon } from "../../assets/images/sidebar/feedback-icon.svg";
import GamingIcon from "../../assets/images/sidebar/gaming-icon.jpg";
import { ReactComponent as HomeIcon } from "../../assets/images/sidebar/home-icon.svg";
import { ReactComponent as LibraryIcon } from "../../assets/images/sidebar/library-icon.svg";
import MusicIcon from "../../assets/images/sidebar/music-icon.jpg";
import NewsIcon from "../../assets/images/sidebar/news-icon.jpg";
import { ReactComponent as PlusIcon } from "../../assets/images/sidebar/plus-icon.svg";
import { ReactComponent as SettingIcon } from "../../assets/images/sidebar/setting-icon.svg";
import { ReactComponent as ShortIcon } from "../../assets/images/sidebar/short-icon.svg";
import SportIcon from "../../assets/images/sidebar/sport-icon.jpg";
import { ReactComponent as SubChannelIcon } from "../../assets/images/sidebar/sub-channel-icon.svg";
import { ReactComponent as SupportIcon } from "../../assets/images/sidebar/support-icon.svg";
import VideoIcon from "../../assets/images/sidebar/video-icon.jpg";
import { ReactComponent as VideoWatchedIcon } from "../../assets/images/sidebar/video-watched-icon.svg";
import { ReactComponent as YoutubeKidsIcon } from "../../assets/images/sidebar/youtube-kids-icon.svg";
import { ReactComponent as YoutubeMusicIcon } from "../../assets/images/sidebar/youtube-music-icon.svg";
import { ReactComponent as YoutubeTvIcon } from "../../assets/images/sidebar/youtube-tv-icon.svg";
import SidebarItem from "../sidebar-item";
import { useStyles } from "./index.style";

const Sidebar = () => {
  const classes = useStyles();
  const smallSidebar = useSelector((state) => state.ui.smallSidebar);
  const loadingVideo = useSelector((state) => state.video.loadingVideo);
  const numberVideo = useSelector((state) => state.video.numberVideo);

  const userInfo =
    localStorage.getItem("user") !== "undefined"
      ? JSON.parse(localStorage.getItem("user"))
      : localStorage.clear;

  if (loadingVideo && typeof numberVideo === "undefined") {
    return (
      <Box className={`${classes.container} ${smallSidebar ? "small" : ""}`}>
        <Box className={classes.section}></Box>
      </Box>
    );
  }

  return (
    <Box className={`${classes.container} ${smallSidebar ? "small" : ""}`}>
      <Box className={classes.section}>
        <Box className={classes.sidebarLists}>
          <Link to="/" style={{ textDecoration: "none", color: "#000000" }}>
            <SidebarItem icon={<HomeIcon />} title={"Trang chủ"} active={true} small={smallSidebar} />
          </Link>
          <SidebarItem icon={<ExploreIcon />} title={"Khám phá"} small={smallSidebar} />
          <SidebarItem icon={<ShortIcon />} title={"Shorts"} small={smallSidebar} />
          <SidebarItem icon={<SubChannelIcon />} title={"Kênh đăng ký"} small={smallSidebar} />
        </Box>

        <Box className={classes.sidebarLists}>
          <SidebarItem icon={<LibraryIcon />} title={"Thư viện"} small={smallSidebar} />
          <SidebarItem icon={<VideoWatchedIcon />} title={"Video đã xem"} small={smallSidebar} />
        </Box>

        {!userInfo && (
          <Box className={classes.sidebarLists}>
            <SidebarItem loginSection={true} />
          </Box>
        )}

        <Box className={classes.sidebarLists}>
          <Box className={classes.sidebarTitle}>Khám phá</Box>
          <SidebarItem icon={<img src={MusicIcon} alt="music-icon" />} title={"Âm nhạc"} />
          <SidebarItem icon={<img src={SportIcon} alt="music-icon" />} title={"Thể thao"} />
          <SidebarItem icon={<img src={GamingIcon} alt="music-icon" />} title={"Trò chơi"} />
          <SidebarItem icon={<img src={NewsIcon} alt="music-icon" />} title={"Tin tức"} />
          <SidebarItem icon={<img src={VideoIcon} alt="music-icon" />} title={"Video 360°"} />
        </Box>

        <Box className={classes.sidebarLists}>
          <SidebarItem icon={<PlusIcon />} title={"Xem qua các kênh"} />
        </Box>

        <Box className={classes.sidebarLists}>
          <Box className={classes.sidebarTitle}>Dịch vụ khác của Youtube</Box>
          <SidebarItem icon={<YoutubeMusicIcon />} title={"Youtube Music"} />
          <SidebarItem icon={<YoutubeKidsIcon />} title={"Youtube Kids"} />
          <SidebarItem icon={<YoutubeTvIcon />} title={"Youtube TV"} />
        </Box>

        <Box className={classes.sidebarLists}>
          <SidebarItem icon={<SettingIcon />} title={"Cài đặt"} />
          <SidebarItem icon={<DiaryIcon />} title={"Nhật kí báo cáo"} />
          <SidebarItem icon={<SupportIcon />} title={"Trợ giúp"} />
          <SidebarItem icon={<FeedbackIcon />} title={"Gửi phản hồi"} />
        </Box>
      </Box>
    </Box>
  );
};

export default Sidebar;
