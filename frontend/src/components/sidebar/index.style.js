import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
  container: {
    width: "240px",
    height: "calc(100vh - 56px)",
    backgroundColor: "rgba(255, 255, 255, 0.98)",
    overflow: "hidden",
    position: "sticky",
    top: "56px",
    overscrollBehavior: "contain",
    "&:hover": {
      overflowY: "auto",
    },
    "&.small": {
      width: "72px",
      "& $section": {
        width: "72px",
        paddingRight: "0",
      },
      "& $sidebarLists:first-child": {
        borderBottom: "none",
        padding: "0",
        marginTop: "5px",
      },
      "& $sidebarLists:nth-child(2)": {
        borderBottom: "none",
        padding: "0",
      },
      // from item 3th to last
      "& $sidebarLists:nth-child(n+3)": {
        display: "none",
      },
    },
    "@media (max-width: 1315px)": {
      width: "72px",
    },
    "@media (max-width: 785px)": {
      display: "none",
    },
  },

  section: {
    width: "240px",
    paddingRight: "12px",
    overscrollBehavior: "contain",
    "@media (max-width: 1315px)": {
      width: "72px",
      paddingRight: "0",
    },
  },

  sidebarLists: {
    padding: "8px 0",
    borderBottom: "1px solid rgb(230, 220, 220)",
    "&:first-child": {
      padding: "12px 0",
      "@media (max-width: 1315px)": {
        borderBottom: "none",
        padding: "0",
        marginTop: "5px",
      },
    },

    "&:last-child": {
      borderBottom: "none",
    },

    "@media (max-width: 1315px)": {
      "&:nth-child(2)": {
        borderBottom: "none",
        padding: "0",
      },
      "&:nth-child(n+3)": {
        display: "none",
      },
    },
  },

  sidebarTitle: {
    color: "#606060",
    padding: "8px 24px",
    fontSize: "14px",
    fontWeight: "500",
    letterSpacing: "0.5px",
    textTransform: "uppercase",
  },
});
