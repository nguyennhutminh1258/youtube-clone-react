export function SmoothHorizontalScrolling(e, time, amount, start) {
  let eAmount = amount / 100;
  let currentTime = 0;
  let scrollCount = 0;
  const y = window.scrollY;
  while (currentTime <= time) {
    window.setTimeout(SHS_B, currentTime, e, scrollCount, eAmount, start, y);
    currentTime += time / 100;
    scrollCount++;
  }
  window.scrollTo(0, y);
}

function SHS_B(e, scrollCount, eAmount, start, y) {
  e.scrollLeft = eAmount * scrollCount + start;
}

export default SmoothHorizontalScrolling;
