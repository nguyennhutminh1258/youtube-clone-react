import { lazy, Suspense, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { setWidth } from "./store/slice/uiSlice";
import ScrollToTop from "./utils/scrollToTop";

const Home = lazy(() => import("./pages/Home"));
const DetailVideo = lazy(() => import("./pages/DetailVideo"));

function App() {
  const dispatch = useDispatch();
  const width = useSelector((state) => state.ui.width);
  const toggle = useSelector((state) => state.ui.toggle);
  const isMobile = navigator.userAgentData.mobile;

  useEffect(() => {
    if (!isMobile) {
      if (toggle) {
        document.body.style.overflow = "hidden";
      }
      if (!toggle) {
        document.body.style.overflow = "unset";
      }
    }
  }, [toggle, isMobile]);

  useEffect(() => {
    function handleResize() {
      dispatch(setWidth(window.innerWidth));
    }
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, [width, dispatch]);

  return (
    <BrowserRouter>
      <ScrollToTop />
      <Suspense fallback="">
        <Routes>
          <Route path="/" element={<Home />} exact />
          <Route path="/detail/:id" element={<DetailVideo />} />
        </Routes>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
